//
//  DateInputView.swift
//  AceDental
//
//  Created by SRS Web Solutions on 27/04/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DateAndTimeInputView: UIView {
    var textField: UITextField!
    var datePicker: UIDatePicker!
    var toolbar: UIToolbar!
    var delegate : UITextFieldDelegate?
    var arrayStates: [String]!
    var dateFormat : String!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.datePicker = UIDatePicker(frame: CGRectMake(0, 0, screenSize.width, 260))
        datePicker.addTarget(self, action: #selector(datePickerDateChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
        datePicker.datePickerMode = UIDatePickerMode.Time
        
        self.toolbar = UIToolbar(frame: CGRectMake(0, 0, screenSize.width, 44))
        
        //        toolbar.translatesAutoresizingMaskIntoConstraints = false
        //        pickerView.translatesAutoresizingMaskIntoConstraints = false
        
//        let buttonDone = UIButton(frame: CGRectMake(0, 0, 80, 44))
//        buttonDone.setTitle("Done", forState: UIControlState.Normal)
//        buttonDone.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
//        buttonDone.addTarget(self, action: "donePressed", forControlEvents: UIControlEvents.TouchUpInside)
        
        //        buttonDone.translatesAutoresizingMaskIntoConstraints = false
        
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(donePressed))
        barbuttonDone.tintColor = UIColor(red: 58/255.0, green: 188/255.0, blue: 1.0, alpha: 1.0)
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil), barbuttonDone]
        
//        let plist = NSBundle.mainBundle().pathForResource("USStateAbbreviations", ofType: "plist")
//        let states = NSDictionary(contentsOfFile: plist!)
//        arrayStates = states?.allKeys.sort({ (obj1, obj2) -> Bool in
//            let state1 = obj1 as! String
//            let state2 = obj2 as! String
//            return state1 < state2
//        }) as! [String]
        
        self.addSubview(datePicker)
        //        self.addSubview(toolbar)
        
        //        let top:NSLayoutConstraint = NSLayoutConstraint(item: toolbar, attribute: NSLayoutAttribute.TopMargin, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0)
        //
        //        let bottom:NSLayoutConstraint = NSLayoutConstraint(item: pickerView, attribute: NSLayoutAttribute.TopMargin, relatedBy: NSLayoutRelation.Equal, toItem: toolbar, attribute: NSLayoutAttribute.BottomMargin, multiplier: 1, constant: 0)
        //
        //        let pickerCenterX:NSLayoutConstraint = NSLayoutConstraint(item: pickerView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0);
        //
        //        let toolCenterX:NSLayoutConstraint = NSLayoutConstraint(item: pickerView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0);
        //
        //        NSLayoutConstraint.activateConstraints([top, bottom, pickerCenterX, toolCenterX])
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @IBAction func datePickerDateChanged(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = dateFormat
        textField.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
    func donePressed() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = dateFormat
        textField.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
        textField.resignFirstResponder()
    }
    
    class func addDatePickerForTextField(textField: UITextField) {
        self.addDatePickerForTextField(textField, minimumDate: nil, maximumDate: nil, dateFormat: nil)
    }
    
    
    
    class func addDatePickerForTextField(textField: UITextField, minimumDate: NSDate?, maximumDate: NSDate?, dateFormat : String?) {
        let dateListView = DateAndTimeInputView(frame: CGRectMake(0, 0, screenSize.width, 260))
        if let format = dateFormat {
            dateListView.dateFormat = format
        } else {
            dateListView.dateFormat = "hh:mm a"
        }
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.minimumDate = minimumDate
        dateListView.datePicker.maximumDate = maximumDate
        
        let dateString = "1 Jan 1980"
        let df = NSDateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.dateFromString(dateString)
        if let unwrappedDate = date {
            dateListView.datePicker.setDate(unwrappedDate, animated: false)
        }

    }
}