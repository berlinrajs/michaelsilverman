//
//  ServiceManager.swift
//   Angell Family Dentistry
//
//  Created by Bose on 02/02/16.
//  Copyright © 2016  Angell Family Dentistry. All rights reserved.
//

import UIKit


class ServiceManager: NSObject {
    
    class func fetchDataFromService(serviceName: String, parameters : [String : String]?, success:(result : AnyObject) -> Void, failure :(error : NSError) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: NSURL(string: "http://mncell.com/mclogin/"))
        
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.POST(serviceName, parameters: parameters, progress: { (progress) in
            print(progress.fractionCompleted)
            }, success: { (task, result) in
                //                print(task)
                //                print(result)
                success(result: result!)
        }) { (task, error) in
            //            print(task)
            //            print(error)
            failure(error: error)
        }
    }
    class func loginWithUsername(userName: String, password: String, completion: (success: Bool, error: NSError?) -> Void) {
        ServiceManager.fetchDataFromService("apploginapi.php?", parameters: ["appkey": "mcangel", "username": userName, "password": password], success: { (result) in
            if (result["posts"]!)!["status"] as! String == "success" {
                completion(success: true, error: nil)
            } else {
                completion(success: false, error: NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"]!)!["message"] as! String]))
            }
        }) { (error) in
            completion(success: false, error: nil)
        }
    }
}
