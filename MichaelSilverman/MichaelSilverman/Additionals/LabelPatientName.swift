//
//  LabelPatientName.swift
//  DistinctiveLaserDentristy
//
//  Created by Bala Murugan on 8/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class LabelPatientName: UILabel {


    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.text = patientName
        
    }
}
