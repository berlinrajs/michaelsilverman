//
//  ChildTreatmentTableViewCell.swift
//  Michael Silverman Dentistry
//
//  Created by Office on 2/23/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ChildTreatmentTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonRound: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
