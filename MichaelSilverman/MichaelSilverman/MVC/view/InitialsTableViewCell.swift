//
//  InitialsTableViewCell.swift
//  MDental
//
//  Created by Office on 2/4/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class InitialsTableViewCell: UITableViewCell {

    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDetails: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        signatureView.layer.cornerRadius = 4.0
        signatureView.layer.borderWidth = 2.0
        signatureView.layer.masksToBounds = true
        signatureView.layer.borderColor = UIColor(white: 1.0, alpha: 0.50).CGColor
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(service : Service) {
        
        let text = service.serviceDescription
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.Justified
        let attributedString = NSAttributedString(string: text,
            attributes: [
                NSParagraphStyleAttributeName: paragraphStyle,
                NSBaselineOffsetAttributeName: NSNumber(float: 0),
                NSFontAttributeName : labelDetails.font
            ])
        labelDetails.attributedText = attributedString
    }

}
