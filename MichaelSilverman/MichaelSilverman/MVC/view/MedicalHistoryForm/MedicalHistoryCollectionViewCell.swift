//
//  MedicalHistoryCollectionViewCell.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/29/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var buttonQuestion: UIButton!
    
    
    func configureCellOption (obj : PDOption) {
        buttonQuestion.setTitle(" " + obj.question, forState: .Normal)
        if let selected = obj.isSelected {
            buttonQuestion.selected = selected
        } else {
            buttonQuestion.selected = false
        }
    }
    
}
