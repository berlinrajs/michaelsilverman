//
//  PDPatient.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDPatient: NSObject {
    var selectedForms : [Forms]!
    var firstName : String!
    var lastName : String!
    var middleInitial : String = ""
    var dateToday : String!
    var doctorName: String!
    var dateOfBirth : String!
    
    var fullName: String {
        return middleInitial == "" ? firstName + " " + lastName : firstName + " " + middleInitial + " " + lastName
    }
    
    
    //NEW PATIENT
    var phoneNumber : String!

    var parentFirstName : String?
    var parentLastName : String?
    var parentRelation: String?

    var parentDateOfBirth : String?
    var addressLine : String!
    var city : String!
    var state : String!
    var zipCode : String!
    var socialSecurityNumber : String?
    var email : String!
    var employerName : String?
    var employerPhoneNumber : String?
    var emergencyContactName : String!
    var emergencyContactPhoneNumber : String!
    var lastDentalVisit : String?
    var reasonForTodayVisit : String?
    var isHavingPain : Bool!
    var painLocation : String?
    var anyConcerns : String?
    var hearAboutUs : String?
    var signature1 : UIImage!
    var signature2 : UIImage!
    var signature3 : UIImage!
    var signature4 : UIImage!
    var signature5 : UIImage!
    var signature6 : UIImage!

    var relationship: String?
    
    var isCallAvail: Bool!
    var isTextAvail: Bool!

    //MEDICAL HISTORY
    var medicalHistoryQuestions1 : [PDQuestion]!
    var medicalHistoryQuestions2 : [PDOption]!
    var medicalHistoryQuestions3 : [PDOption]!
    var medicalHistoryQuestions4 : [PDOption]!
    var isWomen : Bool!
    var controlledSubstances : String?
    var controlledSubstancesClicked : Bool!
    var othersTextForm3 : String?
    var comments : String?
    var otherIllness : String?

    //PATIENT AUTHORIZATION
    var previousClinicName : String!
    var newClinicName : String!
    var patientNumber : String!
    var faxNumber : String?
    var reasonForTransfer : String?

    //SCAN CARDS
    var frontImage: UIImage?
    var backImage: UIImage?
    
    //TOOTH EXTRACTION
    var othersText1 : String?
    var othersText2 : String?
    var toothExtractionQuestions1 : String?
    var toothExtractionQuestions2 : String?
    var prognosisProcedure : String?


    //CHILD TREATMENT
    var selectedOptions : [String]!
    var otherTreatment : String?
    var treatmentDate : String?
    
    var consentFillings: [Service]!
    var oralSurgeryProcedure : String?


    //GUM DISEASE
    var periodontalDate: String?
    var periodontalType: Int?
    var pocketsSelected: Int?
    var teethNumbers: String?
    var agreementSelected: Int?

    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    
}

class Service: NSObject {
    
    var signatureView : SignatureView!
    var serviceDescription : String!
    var index : Int!
    
    override init() {
        super.init()
    }
    
    init(serviceName : String) {
        super.init()
        self.serviceDescription = serviceName
        signatureView = SignatureView()
    }
    
    class func getAllServices() -> [Service] {
        let arrayServices = ["1. FILLINGS:\nI understand that a more extensive restoration than originally place, or possibly root canal therapy, may be required due to addition conditions discovered during tooth preparation. I understand that significant changes in response to temperature may occur after too restoration such  as temporary sensitivity or pain. I also understand that if my tooth does not respond to treatment with a filling, require periodic replacement with additional fillings and/or crowns. I understand I may need further treatment in this office or possibly by a specialist if complications arise during treatment, and any costs thus incurred are my responsibility.",
                             "2. DRUGS AND MEDICATIONS:\nI understand that antibiotics, analgesics, anesthetics and other medications can cause allergic reactions, resulting in redness and swelling of tissues, itching, pain, nausea vomiting or more severe allergic reactions which, although rare, can lead to death. I have informed the doctor of any known allergies. Certain medications may cause drowsiness and it is advisable not to drive or operate hazardous equipment when taking such drugs.",
                             "3. RISKS OF DENTAL ANESTHESIA\nI understand that pain, bruising and occasional temporary or sometimes-permanent numbness in lips, cheeks, tongue or associated facial structures can occur with local anesthetics. About 90% of these cases resolves themselves in less than 8 weeks. Although very rarely needed, a referral to a specialist for evaluation and possibly treatment may be needed if the symptoms do not resolve.",
                             "4. Due to unique differences in each patient’s oral cavity and oral hygiene abilities there is always a risk for relapse,recurrence and/or failure restorations. I understand that it is impossible to predict if and how fast my condition would worsen if untreated, but it is the doctor’s opinion that therapy would be helpful and worsening of the condition(s) would occur sooner without the recommended treatment.",
                             "5. CHANGES IN TREATMENT PLAN:\nI understand that during the course of treatment it may be necessary to change or add procedures because of conditions discovered during treatment that were not evident during examination. I authorize my doctor to use professional judgment to provide appropriate care and understand that the fee proposed is subject to change, depending upon those unforeseen or undiagnosed conditions that may only become apparent once treatment has begun."]
        var serviceList = [Service]()
        for (idx, dict) in arrayServices.enumerate() {
            let obj = Service(serviceName: dict)
            obj.index = idx
            serviceList.append(obj)
        }
        return serviceList
    }
    
}

