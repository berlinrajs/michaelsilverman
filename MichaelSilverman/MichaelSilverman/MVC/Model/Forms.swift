//
//  Forms.swift
//  WestgateSmiles
//
//  Created by samadsyed on 2/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"

let kConsentForms = "CONSENT FORMS"
let kInsuranceCard = "SCAN INSURANCE CARD"
let kDrivingLicense = "SCAN DRIVING LICENSE"
let kNewPatientSignInForm = "NEW PATIENT SIGN-IN FORM"
let kMedicalHistoryForm = "MEDICAL HISTORY FORM"
let kPatientAuthorization = "PATIENT AUTHORIZATION"


let kDentalImplants = "DENTAL IMPLANTS"
let kToothExtraction = "TOOTH EXTRACTION"
let kPartialDenture = "PARTIAL/DENTURE ADJUSTMENTS"
let kInofficeWhitening = "INOFFICE WHITENING"
let kTreatmentOfChild = "TREATMENT OF A MINOR CHILD"
let kEndodonticTreatment = "ENDODONTIC TREATMENT"
let kCrownAndBridges = "CROWNS AND BRIDGES CONSENT FORM"
let kOpiodForm = "OPIOID FORM"
let kGeneralConsent = "GENERAL CONSENT"
let kBracesRemoval = "BRACES REMOVAL AND RETAINER CONSENT FORM"
let kTreatmentFillings = "CONSENT TO TREATMENT FILLING"
let kPhotographyRelease = "PHOTOGRAPHY RELEASE"
let kGumDisease = "NOTIFICATION OF PERIODONTAL (GUM) DISEASE"
let kOralSurgery = "CONSENT FOR ORAL SURGERY"
let kOrthodonticTreatment = "CONSENT TO ORTHODONTIC TREATMENT"
let kPatientHippa = "PATIENT HIPAA CONSENT FORM"
let kInsurance = "INSURANCE RELEASES"



let toothNumberRequired = [kToothExtraction, kEndodonticTreatment, kOpiodForm ]


class Forms: NSObject {
    
    var formTitle : String!
    var subForms : [Forms]!
    var isSelected : Bool!
    var index : Int!
    var toothNumbers : String!
    var isToothNumberRequired : Bool!

    init(formDetails : NSDictionary) {
        super.init()
        self.isSelected = false
    }
    
    override init() {
        super.init()
    }
    
    class func getAllForms (completion :(isConnectionfailed: Bool, forms : [Forms]?) -> Void) {
        let isConnected = Reachability.isConnectedToNetwork()
        let forms = [kNewPatientSignInForm,kMedicalHistoryForm,kPatientAuthorization,kInsuranceCard,kDrivingLicense, kConsentForms]
        let formObj = getFormObjects(forms, isSubForm: false)
        completion(isConnectionfailed: isConnected ? false : true, forms : formObj)
    }

    
    private class func getFormObjects (forms : [String], isSubForm : Bool) -> [Forms] {
        var formList : [Forms]! = [Forms]()
        for (idx, form) in forms.enumerate() {
            let formObj = Forms()
            formObj.isSelected = false
            formObj.index = isSubForm ? idx + 4 : idx
            formObj.formTitle = form
            formObj.isToothNumberRequired = toothNumberRequired.contains(form)
            if formObj.formTitle == kConsentForms {
                formObj.subForms = getFormObjects([kDentalImplants, kToothExtraction, kPartialDenture, kInofficeWhitening, kTreatmentOfChild, kEndodonticTreatment, kCrownAndBridges, kOpiodForm, kGeneralConsent,kBracesRemoval,kTreatmentFillings,kPhotographyRelease,kGumDisease,kOralSurgery,kOrthodonticTreatment,kPatientHippa,kInsurance], isSubForm:  true)
            }
            formList.append(formObj)
        }
        return formList
    }
    
}
