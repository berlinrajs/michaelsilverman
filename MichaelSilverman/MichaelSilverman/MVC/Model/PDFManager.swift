//
//  PDFManager.swift
//   Angell Family Dentistry
//
//  Created by Office on 2/21/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

let kKeychainItemLoginName = "Michael Silverman Dentistry: Google Login"
let kKeychainItemName = "Michael Silverman Dentistry: Google Drive"
let kClientId = "1038936674943-ggsilfn1fqpv0qciio2rcka0m252nmb1.apps.googleusercontent.com"
let kClientSecret = "c1t9y_7tH_AaKkWFs-E1djKG"
private let kFolderName = "MichaelSilverman"


class PDFManager: NSObject {
    
    var service : GTLServiceDrive!
    var credentials : GTMOAuth2Authentication!
    
    
    private func driveService() -> GTLServiceDrive {
        if (service == nil)
        {
            service = GTLServiceDrive()
            
            // Have the service object set tickets to fetch consecutive pages
            // of the feed so we do not need to manually fetch them.
            service.shouldFetchNextPages = true
            
            // Have the service object set tickets to retry temporary error conditions
            // automatically.
            service.retryEnabled = true
        }
        return service
    }
    
    
    func createPDFForView(view : UIView, fileName : String!, patient : PDPatient, completionBlock:(finished : Bool) -> Void) {
        NSOperationQueue.mainQueue().addOperationWithBlock {
            let pdfData = NSMutableData()
            let pageSize = screenSize
            UIGraphicsBeginPDFContextToData(pdfData, CGRectZero, nil)
            let pdfContext : CGContextRef = UIGraphicsGetCurrentContext()!
            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, pageSize.width, pageSize.height), nil)
            view.layer.renderInContext(pdfContext)
            UIGraphicsEndPDFContext()
            do {
                // save as a local file
                let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "MM'_'dd'_'yyyy"
                let dateString = dateFormatter.stringFromDate(NSDate()).uppercaseString
                let name = patient.firstName + "_" + patient.lastName + "_" + dateString + "_" + fileName
                let path = "\(documentsPath)/\(name).pdf"
                try pdfData.writeToFile(path, options: .DataWritingAtomic)
                self.uploadFileToDrive(path, fileName: name)
                completionBlock(finished: true)
            } catch _ as NSError {
                completionBlock(finished: false)
            }
        }
        
    }
    
    
    func createPDFForScrollView(scrollView : UIScrollView, fileName : String, patient : PDPatient, completionBlock:(finished : Bool) -> Void) {
        NSOperationQueue.mainQueue().addOperationWithBlock {
            let pdfData = NSMutableData()
            let scrollHeight = scrollView.contentSize.height
            let rawNumberOfPages = scrollHeight / screenSize.height
            let numberOfPages = Int(ceil(rawNumberOfPages))
            var pageNumber = Int()
            let pageSize = screenSize
            scrollView.setContentOffset(CGPointMake(0, 0), animated: false)
            UIGraphicsBeginPDFContextToData(pdfData, CGRectZero, nil)
            let pdfContext : CGContextRef = UIGraphicsGetCurrentContext()!
            repeat {
                UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, pageSize.width, pageSize.height), nil)
                if pageNumber < 1 {
                    scrollView.layer.renderInContext(pdfContext)
                } else if pageNumber >= 1 {
                    let offsetForScroll = CGFloat(pageNumber) * screenSize.height
                    scrollView.setContentOffset(CGPointMake(0, offsetForScroll), animated: false)
                    CGContextTranslateCTM(UIGraphicsGetCurrentContext()!, 0, -offsetForScroll)
                    scrollView.layer.renderInContext(pdfContext)
                }
                pageNumber = pageNumber + 1
            }
                while pageNumber < numberOfPages
            UIGraphicsEndPDFContext()
            do {
                // save as a local file
                let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "MM'_'dd'_'yyyy"
                let dateString = dateFormatter.stringFromDate(NSDate()).uppercaseString
                let name = patient.firstName + "_" + patient.lastName + "_" + dateString + "_" + fileName
                let path = "\(documentsPath)/\(name).pdf"
                try pdfData.writeToFile(path, options: .DataWritingAtomic)
                self.uploadFileToDrive(path, fileName: name)
                completionBlock(finished: true)
            } catch _ as NSError {
                completionBlock(finished: false)
            }
        }
    }
    
    
    ///MARK:- CHECK DRIVE FREE SPACE
    func CheckGoogleDriveFreeSpace()  {
        let query : GTLQueryDrive = GTLQueryDrive.queryForAboutGet()
        query.fields = "storageQuota,user"
        _  = driveService().executeQuery(query) { (ticket, about, error) in
            if error == nil{
                let abt : GTLDriveAbout = about as! GTLDriveAbout
                let limitInGB : Int = Int(abt.storageQuota.limit.doubleValue/1024.0/1024.0/1024.0)
                let usageinGB : Int = Int(abt.storageQuota.usage.doubleValue/1024.0/1024.0/1024.0)
                let userEmail : String = abt.user.emailAddress
                
                //ALMOST FULL
                if usageinGB > limitInGB - 1{
                    self.showGoogleDriveErrorAlert()
                    
                }else if (usageinGB/limitInGB) * 100 > 80{
                    /// 80% OF DRIVE IS FULL
                    self.sendWarningEmail(userEmail)
                    
                }
            }else{
                print("Error \(error.localizedDescription)")
            }
            
        }
    }
    
    //MARK:- GOOGLE DRIVE ERROR ALERT
    func showGoogleDriveErrorAlert () {
        let alertController = UIAlertController(title: "WARNING", message: "YOUR GOOGLE DRIVE IS ALMOST FULL", preferredStyle: UIAlertControllerStyle.Alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Destructive) { (action) -> Void in
        }
        alertController.addAction(alertOkAction)
        (AppDelegate.sharedInstance().window?.rootViewController as! UINavigationController).viewControllers.last!.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    //MARK:- SEND WARNING EMAIL
    func sendWarningEmail(toEmail : String){
        let message : SMTPMessage = SMTPMessage()
        message.from = "demo@srswebsolutions.com"
        message.to = toEmail
        message.host = "smtp.gmail.com"
        message.account = "demo@srswebsolutions.com"
        message.pwd = "Srsweb123#"
        
        message.subject = "Your Google drive almost Full"
        message.content = "<p>Hi,</p><p>Your google drive account is 80% full. So please clear some contents immediately</p><p>Thank you.</p>"
        message.send({ (messg, now, total) in
            
            }, success: { (messg) in
                print("Email sent")
            }, failure: { (messg, error) in
        })
        
    }
    
    
    func uploadFileToDrive(path : String, fileName : String) {
        self.CheckGoogleDriveFreeSpace()
        func uploadFile(identitifer : String) {
            let driveFile = GTLDriveFile()
            driveFile.mimeType = "application/pdf"
            driveFile.originalFilename = "\(fileName).pdf"
            driveFile.name = "\(fileName).pdf"
            driveFile.parents = [identitifer]
            
            let uploadParameters = GTLUploadParameters(data: NSData(contentsOfFile: path)!, MIMEType: "application/pdf")
            let query = GTLQueryDrive.queryForFilesCreateWithObject(driveFile, uploadParameters: uploadParameters)
            query.addParents = identitifer
            
            self.driveService().executeQuery(query, completionHandler: { (ticket, uploadedFile, error) -> Void in
                if (error == nil) {
                    let fileManager = NSFileManager.defaultManager()
                    if fileManager.fileExistsAtPath(path) {
                        do {
                            try fileManager.removeItemAtPath(path)
                        } catch  {
                            
                        }
                    }
                } else {
                }
            })
        }
        
        func createFolder(folderName : String, parent : [String]?) {
            let folderObj = GTLDriveFile()
            folderObj.name = folderName
            if parent != nil {
                folderObj.parents = parent
            }
            folderObj.mimeType = "application/vnd.google-apps.folder"
            
            let queryFolder = GTLQueryDrive.queryForFilesCreateWithObject(folderObj, uploadParameters: nil)
            
            self.driveService().executeQuery(queryFolder, completionHandler: { (ticket, result, error) -> Void in
                if (error == nil) {
                    let folder = result as! GTLDriveFile
                    if parent == nil {
                        checkAndCreateSubFolder(folder)
                    } else {
                        uploadFile(folder.identifier)
                    }
                } else {
                    
                }
            })
        }
        
        func checkAndCreateSubFolder(folder : GTLDriveFile) {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "MM'-'dd'-'yyyy"
            let folderName = dateFormatter.stringFromDate(NSDate()).uppercaseString
            
            let folderDateQuery = GTLQueryDrive.queryForFilesList()
            folderDateQuery.q = "mimeType = 'application/vnd.google-apps.folder' and name = '\(folderName)' and trashed = false and '\(folder.identifier)' in parents"
            
            self.driveService().executeQuery(folderDateQuery, completionHandler: { (ticket, obj, error) -> Void in
                if error == nil {
                    let childFolder = (obj as! GTLDriveFileList).files
                    if childFolder != nil && childFolder.count > 0 {
                        let dateFolder = childFolder[0] as! GTLDriveFile
                        uploadFile(dateFolder.identifier)
                    } else {
                        createFolder(folderName, parent: [folder.identifier])
                    }
                } else {
                }
            })
        }
        
        let folderQuery = GTLQueryDrive.queryForFilesList()
        folderQuery.q = "mimeType = 'application/vnd.google-apps.folder' and trashed = false"
        
        self.driveService().executeQuery(folderQuery, completionHandler: { (ticker, folder, error) -> Void in
            if (error == nil) {
                let fileList = (folder as! GTLDriveFileList).files
                let name = "\(UIDevice.currentDevice().name)_" + kFolderName
                if fileList != nil {
                    let arrayFiltered = fileList.filter({ (driveFolders) -> Bool in
                        let folder = driveFolders as! GTLDriveFile
                        return folder.name == name
                    })
                    if arrayFiltered.count > 0 {
                        let folder = arrayFiltered[0] as! GTLDriveFile
                        checkAndCreateSubFolder(folder)
                        
                    } else {
                        createFolder(name, parent: nil)
                    }
                } else {
                    createFolder(name, parent: nil)
                }
            } else {
            }
        })
    }
    func authorizeDrive(view : UIView, completion:(success : Bool) -> Void) {
        credentials = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychainForName(kKeychainItemName, clientID: kClientId, clientSecret: kClientSecret)
        if credentials.canAuthorize {
            self.driveService().authorizer = credentials
            completion(success: true)
        } else {
            let authViewController = GTMOAuth2ViewControllerTouch(scope: kGTLAuthScopeDriveFile, clientID: kClientId, clientSecret: kClientSecret, keychainItemName: kKeychainItemName, completionHandler: { (controller, auth, error) -> Void in
                controller.view.removeFromSuperview()
                if error == nil {
                    self.driveService().authorizer = auth
                    completion(success: true)
                } else {
                    completion(success: false)
                }
            })
            authViewController.view.frame = view.frame
            view.addSubview(authViewController.view)
        }
    }
    
}
