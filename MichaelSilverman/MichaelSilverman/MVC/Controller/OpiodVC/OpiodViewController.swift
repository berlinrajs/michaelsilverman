//
//  OpiodViewController.swift
//  Michael Silverman Dentistry
//
//  Created by SRS Web Solutions on 05/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class OpiodViewController: PDViewController {

    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let form = patient.selectedForms.first!//patient.selectedForms.last!.formTitle == kOpiodForm ? patient.selectedForms.last! : patient.selectedForms[patient.selectedForms.count - 2]
        if form.toothNumbers != nil {
            let string = "Please consider this information carefully before agreeing to take your \(form.toothNumbers!) prescription."
            let attString = NSMutableAttributedString(string: string)
            attString.addAttributes([NSUnderlineStyleAttributeName: 1], range: (string as NSString).rangeOfString(form.toothNumbers!))
            labelDetails.attributedText = attString
        }

        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(OpiodViewController.labelDateTapped(_:)))
        tapgesture.numberOfTapsRequired = 1
        labelDate.addGestureRecognizer(tapgesture)
        
        labelName.text = patient.fullName//"\(patient.firstName) \(patient.lastName)"
        
        // Do any additional setup after loading the view.
    }
    
    func labelDateTapped(sender: AnyObject) {
        self.labelDate.text = patient.dateToday
        self.labelDate.textColor = UIColor.blackColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func nextAction(sender: AnyObject) {
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if labelDate.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.signature1 = signatureView.signatureImage()
            let formVC = self.storyboard?.instantiateViewControllerWithIdentifier("kOpiodFormViewController") as! OpiodFormViewController
            formVC.patient = self.patient
            navigationController?.pushViewController(formVC, animated: true)
        }
    }
    

}
