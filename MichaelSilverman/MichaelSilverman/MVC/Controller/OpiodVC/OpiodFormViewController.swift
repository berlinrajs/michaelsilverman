//
//  OpiodFormViewController.swift
//  Michael Silverman Dentistry
//
//  Created by SRS Web Solutions on 05/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class OpiodFormViewController: PDViewController {

    
    @IBOutlet weak var labelName: FormLabel!
    @IBOutlet weak var labelDate: FormLabel!
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var signatureView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()

        // Do any additional setup after loading the view.
    }

    func loadValues() {
        let form = patient.selectedForms.first!//patient.selectedForms.last!.formTitle == kOpiodForm ? patient.selectedForms.last! : patient.selectedForms[patient.selectedForms.count - 2]
        if form.toothNumbers != nil {
            let string = "Please consider this information carefully before agreeing to take your \(form.toothNumbers!) prescription."
            let attString = NSMutableAttributedString(string: string)
            attString.addAttributes([NSUnderlineStyleAttributeName: 1], range: (string as NSString).rangeOfString(form.toothNumbers!))
            labelDetails.attributedText = attString
        }
        labelName.text = "\(patient.firstName) \(patient.lastName)"
        
        signatureView.image = patient.signature1
        labelDate.text = patient.dateToday
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
