//
//  InOfficeWhiteningViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Leojin Bose on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class InOfficeWhiteningViewController: PDViewController {

    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var labelDate1: PDLabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let patientName = patient.fullName//"\(patient.firstName) \(patient.lastName)"
        labelName.text = patientName
        signatureView1.layer.cornerRadius = 3.0
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(InOfficeWhiteningViewController.setDateOnLabel1))
        tapGesture1.numberOfTapsRequired = 1
        labelDate1.addGestureRecognizer(tapGesture1)
        
        // Do any additional setup after loading the view.
    }
    
    func setDateOnLabel1() {
        labelDate1.text = patient.dateToday
        labelDate1.textColor = UIColor.blackColor()
    }
    

    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        if !signatureView1.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if labelDate1.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.signature1 = signatureView1.signatureImage()
            let inOfficeWhiteningFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kInOfficeWhiteningFormVC") as! InOfficeWhiteningFormViewController
            inOfficeWhiteningFormVC.patient = patient
            self.navigationController?.pushViewController(inOfficeWhiteningFormVC, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
