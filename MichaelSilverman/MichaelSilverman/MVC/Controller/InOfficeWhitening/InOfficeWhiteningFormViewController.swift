//
//  InOfficeWhiteningFormViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Leojin Bose on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class InOfficeWhiteningFormViewController: PDViewController {

    var textRanges : [NSRange]! = [NSRange]()

    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var imageViewSignature1: UIImageView!
    @IBOutlet weak var labelDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageViewSignature1.image = patient.signature1
        labelDate1.text = patient.dateToday
        // Do any additional setup after loading the view.
        
        var patientInfo = "I"
        
        let patientName = patient.fullName//getText("\(patient.firstName) \(patient.lastName)")
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " D.O.B: \(getText(patient.dateOfBirth))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateOfBirth)))
        
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range)
        }

        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(float: 0)], range: NSMakeRange(0, attributedString.length))
        labelDetails.attributedText = attributedString
        
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
