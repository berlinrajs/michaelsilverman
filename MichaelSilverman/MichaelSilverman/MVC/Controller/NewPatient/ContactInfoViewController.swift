//
//  ContactInfoViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ContactInfoViewController: PDViewController {
    @IBOutlet weak var textFieldEmployer: PDTextField!
    @IBOutlet weak var textFieldEmployerPhoneNumber: PDTextField!
    @IBOutlet weak var textFieldEmergencyContact: PDTextField!
    @IBOutlet weak var textFieldEmergencyContactNumber: PDTextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        if !textFieldEmployerPhoneNumber.isEmpty && !textFieldEmployerPhoneNumber.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID EMPLOYER PHONE NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
//        } else if textFieldEmergencyContact.isEmpty {
//            let alert = Extention.alert("PLEASE ENTER EMERGENCY CONTACT NAME")
//            self.presentViewController(alert, animated: true, completion: nil)
  //      }
 //           else if !textFieldEmergencyContactNumber.isEmpty && !textFieldEmergencyContactNumber.text!.isPhoneNumber {
//            let alert = Extention.alert("PLEASE ENTER EMERGENCY CONTACT NUMBER")
//            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldEmergencyContactNumber.isEmpty && !textFieldEmergencyContactNumber.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID EMERGENCY CONTACT NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            if !textFieldEmployer.isEmpty {
                patient.employerName = textFieldEmployer.text
            } else {
                patient.employerName = nil
            }
            if !textFieldEmployerPhoneNumber.isEmpty {
                patient.employerPhoneNumber = textFieldEmployerPhoneNumber.text
            } else {
                patient.employerPhoneNumber = nil
            }
            patient.emergencyContactName = textFieldEmergencyContact.isEmpty ? "" : textFieldEmergencyContact.text
            patient.emergencyContactPhoneNumber = textFieldEmergencyContactNumber.text
            let diseaseInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDiseaseInfoVC") as! DiseaseInfoViewController
            diseaseInfoVC.patient = patient
            self.navigationController?.pushViewController(diseaseInfoVC, animated: true)
        }
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

}

extension ContactInfoViewController : UITextFieldDelegate {
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == textFieldEmergencyContactNumber || textField == textFieldEmployerPhoneNumber) {
            
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
