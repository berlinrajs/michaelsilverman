//
//  NewPatientViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class NewPatientViewController: PDViewController {
    var textRanges : [NSRange]! = [NSRange]()
    @IBOutlet weak var labelPatientDetails: UILabel!
    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var labelDate2: UILabel!
    @IBOutlet weak var labelText2: UILabel!
    @IBOutlet weak var labelText1: UILabel!
    @IBOutlet weak var imageViewSignature1: UIImageView!
    @IBOutlet weak var imageViewSignature2: UIImageView!
    @IBOutlet weak var constraintPatientDetailsHeight: NSLayoutConstraint!
    
    @IBOutlet weak var constraintLabel2Height: NSLayoutConstraint!
    @IBOutlet weak var constraintLabel1Height: NSLayoutConstraint!
    
    
    @IBOutlet weak var imageViewSignature3: UIImageView!
    @IBOutlet weak var imageViewSignature4: UIImageView!
    @IBOutlet weak var imageViewSignature5: UIImageView!
    
    @IBOutlet weak var labelDate3: UILabel!
    @IBOutlet weak var labelDate4: UILabel!
    @IBOutlet weak var labelDate5: UILabel!
    
    @IBOutlet weak var labelPhoneNumber: UILabel!
    @IBOutlet weak var buttonCall: UIButton!
    @IBOutlet weak var buttonText: UIButton!
    @IBOutlet weak var labelName2: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var patientInfo = "Patient Name:"
        
        let patientName = patient.fullName//getText("\(patient.firstName) \(patient.lastName)")
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " Date of Birth: \(getText(patient.dateOfBirth))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateOfBirth)))
        let text = getText("N/A")

        if let parentFirstName = patient.parentFirstName {
            let parentName = getText("\(parentFirstName) \(patient.parentLastName!)")
            patientInfo = patientInfo + " Parent/Guardian: \(parentName)"
            textRanges.append(patientInfo.rangeOfText(parentName))
            
            patientInfo = patientInfo + " Date of Birth: \(getText(patient.parentDateOfBirth!))"
            textRanges.append(patientInfo.rangeOfText(getText(patient.parentDateOfBirth!)))
        } else {
            patientInfo = patientInfo + " Parent/Guardian: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))
            
            patientInfo = patientInfo + " Date of Birth: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))
        }
        
        patientInfo = patientInfo + " Address: \(getText(patient.addressLine))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.addressLine)))
        
        patientInfo = patientInfo + " City: \(getText(patient.city))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.city)))
        
        patientInfo = patientInfo + " State: \(getText(patient.state))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.state)))
        
        patientInfo = patientInfo + " Zip Code: \(getText(patient.zipCode))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.zipCode)))
        
        if let securityNumber = patient.socialSecurityNumber {
            patientInfo = patientInfo + " Social Security #(required for all pt's 18 and over): \(getText(securityNumber))"
            textRanges.append(patientInfo.rangeOfText(getText(securityNumber)))
        } else {
            patientInfo = patientInfo + " Social Security #(required for all pt's 18 and over): \(text)"
            textRanges.append(patientInfo.rangeOfText(text))
        }
        
        
        
        patientInfo = patientInfo + " Ph #: \(getText(patient.phoneNumber))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.phoneNumber)))
        
        patientInfo = patientInfo + " Email: \(getText(patient.email))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.email)))
        
        if let employer = patient.employerName {
            patientInfo = patientInfo + " Employer: \(getText(employer))"
            textRanges.append(patientInfo.rangeOfText(getText(employer)))
        } else {
            patientInfo = patientInfo + " Employer: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))

        }
        
        if let employerContact = patient.employerPhoneNumber {
            patientInfo = patientInfo + " Phone Number: \(getText(employerContact))"
            textRanges.append(patientInfo.rangeOfText(getText(employerContact)))
        } else {
            patientInfo = patientInfo + " Phone Number: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))
        }
        
        patientInfo = patientInfo + " Emergency Contact: \(getText(patient.emergencyContactName))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.emergencyContactName)))
        
        patientInfo = patientInfo + " Phone Number: \(getText(patient.emergencyContactPhoneNumber))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.emergencyContactPhoneNumber)))
        
        if let lastVisit = patient.lastDentalVisit {
            patientInfo = patientInfo + " Last Dental Visit: \(getText(lastVisit))"
            textRanges.append(patientInfo.rangeOfText(getText(lastVisit)))
        } else {
            patientInfo = patientInfo + " Last Dental Visit: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))

        }
        
        if let reason = patient.reasonForTodayVisit {
            patientInfo = patientInfo + " Reason for today's Dental Visit: \(getText(reason))"
            textRanges.append(patientInfo.rangeOfText(getText(reason)))
        } else {
            patientInfo = patientInfo + " Reason for today's Dental Visit: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))

        }
        
        if patient.isHavingPain == true {
            if let painLocation = patient.painLocation {
                patientInfo = patientInfo + " Having pain? YES  If Yes where: \(getText(painLocation))"
                textRanges.append(patientInfo.rangeOfText(getText(painLocation)))
            } else {
                patientInfo = patientInfo + " Having pain? YES  If Yes where: \(text)"
                textRanges.append(patientInfo.rangeOfText(text))

            }
        } else {
            patientInfo = patientInfo + " Having pain? NO  If Yes where: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))

        }
        
        if let concerns = patient.anyConcerns {
            patientInfo = patientInfo + " Any concerns: \(getText(concerns))"
            textRanges.append(patientInfo.rangeOfText(getText(concerns)))
        } else {
            patientInfo = patientInfo + " Any concerns: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))

        }
        
        // Do any additional setup after loading the view.
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range)
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 8
        paragraphStyle.alignment = NSTextAlignment.Justified
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(float: 0), NSParagraphStyleAttributeName : paragraphStyle], range: NSMakeRange(0, attributedString.length))
        labelPatientDetails.attributedText = attributedString
        
        var rect = labelPatientDetails.frame
        labelPatientDetails.sizeToFit()
        rect.size.height = CGRectGetHeight(labelPatientDetails.frame)
        labelPatientDetails.frame = rect
        
        imageViewSignature1.image = patient.signature1
        imageViewSignature2.image = patient.signature2
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelText1.setAttributedText()
        labelText2.setAttributedText()
        
        constraintPatientDetailsHeight?.constant = labelPatientDetails.attributedText!.heightWithConstrainedWidth(screenSize.width - 60.0) * 2
        constraintLabel1Height?.constant = labelText1.attributedText!.heightWithConstrainedWidth(screenSize.width - 60.0)
        constraintLabel2Height?.constant = labelText2.attributedText!.heightWithConstrainedWidth(screenSize.width - 60.0)
        
        
        imageViewSignature3.image = patient.signature3
        imageViewSignature4.image = patient.signature4
        imageViewSignature5.image = patient.signature5
        labelDate3.text = patient.dateToday
        labelDate4.text = patient.dateToday
        labelDate5.text = patient.dateToday
        labelPhoneNumber.text = patient.phoneNumber
        buttonCall.selected = patient.isCallAvail
        buttonText.selected = patient.isTextAvail
        labelName2.text = patient.fullName//patient.firstName + " " + patient.lastName
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
