//
//  AddressInfoViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AddressInfoViewController: PDViewController {
    
    
    @IBOutlet weak var textFieldAddressLine: PDTextField!
    
    @IBOutlet weak var textFieldState: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldZipCode: PDTextField!
    @IBOutlet weak var textFieldEmail: PDTextField!
    @IBOutlet weak var textFieldPhone: PDTextField!
    @IBOutlet weak var textFieldSecurityNumber: PDTextField!
    
    var arrayStates : [String] = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        StateListView.addStateListForTextField(textFieldState)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        if let _ = findEmptyTextField() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldZipCode.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID ZIPCODE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldSecurityNumber.isEmpty && textFieldSecurityNumber.text!.characters.count != 9 {
            let alert = Extention.alert("PLEASE ENTER VALID SOCIAL SECURITY NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldPhone.isEmpty && !textFieldPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldEmail.isEmpty && !textFieldEmail.text!.isValidEmail {
            let alert = Extention.alert("PLEASE ENTER VALID EMAIL")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.addressLine = (textFieldAddressLine.text!)
            patient.state = textFieldState.text
            if !textFieldSecurityNumber.isEmpty {
                patient.socialSecurityNumber = textFieldSecurityNumber.text
            } else {
                patient.socialSecurityNumber = nil
            }
            
            patient.zipCode = textFieldZipCode.text
            patient.city = textFieldCity.text
            patient.email = textFieldEmail.text?.lowercaseString
            patient.phoneNumber = textFieldPhone.text
            let contactInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kContactInfoVC") as! ContactInfoViewController
            contactInfoVC.patient = patient
            self.navigationController?.pushViewController(contactInfoVC, animated: true)
        }
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func findEmptyTextField() -> UITextField? {
        let textFields = is18YearsOld ? [textFieldCity, textFieldState, textFieldZipCode, textFieldAddressLine] : [textFieldCity, textFieldState, textFieldZipCode, textFieldAddressLine]
        for textField in textFields {
            if textField.isEmpty {
                return textField
            }
        }
        return nil
    }
    
    var is18YearsOld : Bool {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        let birthDate = dateFormatter.dateFromString(patient.dateOfBirth.capitalizedString)
        let ageComponents = NSCalendar.currentCalendar().components(.Year, fromDate: birthDate!, toDate: NSDate(), options: NSCalendarOptions(rawValue: 0))
        return ageComponents.year >= 18
    }
}

extension AddressInfoViewController : UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textFieldZipCode {
            return textField.formatZipCode(range, string: string)
        }
        
        if textField == textFieldSecurityNumber {
            return textField.formatNumbers(range, string: string, count: 9)
        }
        if textField == textFieldPhone {
           
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
