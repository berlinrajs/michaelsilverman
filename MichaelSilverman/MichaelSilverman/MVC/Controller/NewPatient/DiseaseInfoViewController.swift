//
//  DiseaseInfoViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DiseaseInfoViewController: PDViewController {
    
    @IBOutlet weak var textViewReasonForTodayDentalVisit: PDTextView!
    @IBOutlet weak var textFieldLastDentalVisit: PDTextField!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var toolBar: UIToolbar!

    @IBOutlet weak var buttonConcerns: RadioButton!
    @IBOutlet weak var radioButtonPain: RadioButton!
    
    @IBOutlet var viewPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textViewOthers: PDTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.maximumDate = NSDate()

        
        textFieldLastDentalVisit.inputView = datePicker
        textFieldLastDentalVisit.inputAccessoryView = toolBar
        let dateString = "1 Jan 2016"
        let df = NSDateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.dateFromString(dateString)
        if let unwrappedDate = date {
            datePicker.setDate(unwrappedDate, animated: false)
        }

       // textFieldLastDentalVisit.placeholder = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        
        if !textFieldLastDentalVisit.isEmpty {
            patient.lastDentalVisit = textFieldLastDentalVisit.text
        } else {
            patient.lastDentalVisit = nil
        }
        if textViewReasonForTodayDentalVisit.text != "Briefly explain" {
            patient.reasonForTodayVisit = textViewReasonForTodayDentalVisit.text
        } else {
            patient.reasonForTodayVisit = nil
        }
        patient.isHavingPain = radioButtonPain.selectedButton.tag == 1
        let agreementVC = self.storyboard?.instantiateViewControllerWithIdentifier("kAgreementVC") as! AgreementViewController
        agreementVC.patient = patient
        self.navigationController?.pushViewController(agreementVC, animated: true)
    }
    
    
    @IBAction func buttonActionYesNo(sender: RadioButton) {
        if sender.tag == 1 {
            showPopup(1)
        }else{
            patient.painLocation = nil
        }
    }
    
    
    @IBAction func buttonActionConcerns(sender: RadioButton) {
        if sender.tag == 1 {
            showPopup(2)
        }
        else
        {
            patient.anyConcerns = nil
        }
    }
    
    @IBAction func buttonActionPopupOk(sender: AnyObject) {
        if textViewOthers.tag == 1 {
            if !textViewOthers.isEmpty && textViewOthers.text != "IF YES, WHERE" {
                patient.painLocation = textViewOthers.text
            } else {
                patient.painLocation = nil
                radioButtonPain.setSelectedWithTag(2)
            }
        } else {
            if !textViewOthers.isEmpty && textViewOthers.text != "IF YES TYPE HERE" {
                patient.anyConcerns = textViewOthers.text
            } else {
                patient.anyConcerns = nil
                buttonConcerns.setSelectedWithTag(2)
            }
        }
        textViewOthers.resignFirstResponder()
        self.viewPopup.removeFromSuperview()
        self.viewShadow.hidden = true
    }
    
    func showPopup(tag : Int) {
        let frameSize = CGRectMake(0, 0, 348, 174)
        self.viewPopup.frame = frameSize
        self.viewPopup.center = self.view.center
        self.viewShadow.addSubview(self.viewPopup)
        self.viewPopup.transform = CGAffineTransformMakeScale(0.1, 0.1)
        textViewOthers.text = tag == 1 ? "IF YES, WHERE" : "IF YES TYPE HERE"
        textViewOthers.tag = tag
        textViewOthers.textColor = UIColor.lightGrayColor()
        self.viewShadow.hidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewPopup.transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }

    
    @IBAction func toolbarDoneButtonAction(sender: AnyObject) {
        textFieldLastDentalVisit.resignFirstResponder()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldLastDentalVisit.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
    
    @IBAction func datePickerDateChanged(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldLastDentalVisit.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }

}


extension DiseaseInfoViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        
        if textView == textViewOthers {
            if textView.text == "IF YES TYPE HERE" || textView.text == "IF YES, WHERE" {
                textView.text = ""
                textView.textColor = UIColor.blackColor()
            }
        } else {
            if textView.text == "Briefly explain" {
                textView.text = ""
                textView.textColor = UIColor.whiteColor()
            }
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = textView == textViewOthers ? textViewOthers.tag == 1 ? "IF YES, WHERE" : "IF YES TYPE HERE" : "Briefly explain"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}