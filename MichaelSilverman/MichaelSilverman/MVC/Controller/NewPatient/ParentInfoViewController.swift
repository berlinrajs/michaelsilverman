//
//  ParentInfoViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ParentInfoViewController: PDViewController {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    @IBOutlet weak var textFieldDateOfBirth: PDTextField!
    @IBOutlet weak var buttonYes: UIButton!
    @IBOutlet weak var buttonNo: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // datePicker.maximumDate = NSDate()
        let dateString = "1 Jan 1980"
        let df = NSDateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.dateFromString(dateString)
        if let unwrappedDate = date {
            datePicker.setDate(unwrappedDate, animated: false)
        }

        textFieldDateOfBirth.inputView = datePicker
        textFieldDateOfBirth.inputAccessoryView = toolBar
        labelDate.text = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        func gotoAddressInfoVC() {
            let addressInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kAddressInfoVC") as! AddressInfoViewController
            addressInfoVC.patient = patient
            self.navigationController?.pushViewController(addressInfoVC, animated: true)
        }
        
        self.view.endEditing(true)
        if buttonYes.selected {
            if textFieldFirstName.isEmpty {
                let alert = Extention.alert("PLEASE ENTER FIRST NAME")
                self.presentViewController(alert, animated: true, completion: nil)
            } else if textFieldLastName.isEmpty {
                let alert = Extention.alert("PLEASE ENTER LAST NAME")
                self.presentViewController(alert, animated: true, completion: nil)
            } else if textFieldDateOfBirth.isEmpty {
                let alert = Extention.alert("PLEASE ENTER DATE OF BIRTH")
                self.presentViewController(alert, animated: true, completion: nil)
            } else {
                patient.parentFirstName = textFieldFirstName.text
                patient.parentLastName = textFieldLastName.text
                patient.parentDateOfBirth = textFieldDateOfBirth.text
                gotoAddressInfoVC()
            }
        } else {
            gotoAddressInfoVC()
        }
    }
    

    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func buttonActionYesNo(sender: UIButton) {
        sender.selected = true
        if sender == buttonYes {
            buttonNo.selected = false
            textFieldFirstName.enabled = true
            textFieldLastName.enabled = true
            textFieldDateOfBirth.enabled = true
        } else {
            buttonYes.selected = false
            textFieldFirstName.enabled = false
            textFieldLastName.enabled = false
            textFieldDateOfBirth.enabled = false
            textFieldLastName.text = ""
            textFieldFirstName.text = ""
            textFieldDateOfBirth.text = ""
        }
    }
    
    
    @IBAction func toolbarDoneButtonAction(sender: AnyObject) {
        textFieldDateOfBirth.resignFirstResponder()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldDateOfBirth.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
    
    @IBAction func datePickerDateChanged(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldDateOfBirth.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }

}
