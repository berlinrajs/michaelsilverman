//
//  AgreementViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AgreementViewController: PDViewController {
    
    @IBOutlet weak var labelText1: UILabel!
    @IBOutlet weak var labelText2: UILabel!
    @IBOutlet weak var constraintLabel1Height: NSLayoutConstraint!
    @IBOutlet weak var constraintLabel2Height: NSLayoutConstraint!
    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var signatureView2: SignatureView!
    @IBOutlet weak var labelDate1: PDLabel!
    @IBOutlet weak var labelDate2: PDLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signatureView1.layer.cornerRadius = 3.0
        signatureView2.layer.cornerRadius = 3.0
        labelText1.setAttributedText()
        labelText2.setAttributedText()
        labelPatientName.text = patient.fullName
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(AgreementViewController.setDateOnLabel1))
        tapGesture1.numberOfTapsRequired = 1
        labelDate1.addGestureRecognizer(tapGesture1)
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(AgreementViewController.setDateOnLabel2))
        tapGesture2.numberOfTapsRequired = 1
        labelDate2.addGestureRecognizer(tapGesture2)
        // Do any additional setup after loading the view.
    }
    
    func setDateOnLabel1() {
        labelDate1.text = patient.dateToday
        labelDate1.textColor = UIColor.blackColor()
    }

    func setDateOnLabel2() {
        labelDate2.text = patient.dateToday
        labelDate2.textColor = UIColor.blackColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        if !signatureView1.isSigned() || !signatureView2.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if labelDate1.text == "Tap to date" || labelDate2.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.signature1 = signatureView1.signatureImage()
            patient.signature2 = signatureView2.signatureImage()
            let hippaStep1 = self.storyboard?.instantiateViewControllerWithIdentifier("kHippaStep1VC") as! HippaStep1VC
            hippaStep1.patient = patient
            self.navigationController?.pushViewController(hippaStep1, animated: true)
        }
    }

}
