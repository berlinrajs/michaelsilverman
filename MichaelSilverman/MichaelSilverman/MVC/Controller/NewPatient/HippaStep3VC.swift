//
//  HippaStep3VC.swift
//  Michael Silverman Dentistry
//
//  Created by SRS Web Solutions on 05/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HippaStep3VC: PDViewController {

    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var signatureView: SignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelName.text = "I, \(patient.fullName), have reviewed a copy of this office’s Notice of Privacy Practices and consent to photos for patient file and/or of treatment."

        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func actionNext(sender: AnyObject) {
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.signature5 = signatureView.signatureImage()
            let newPatientVC = self.storyboard?.instantiateViewControllerWithIdentifier("kNewPatientVC") as! NewPatientViewController
            newPatientVC.patient = patient
            self.navigationController?.pushViewController(newPatientVC, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
