//
//  ChildTreatmentViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Leojin Bose on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ChildTreatmentViewController: PDViewController {

    var textRanges : [NSRange]! = [NSRange]()
    var selectedOptions : [String]! = [String]()
    
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet weak var textFieldDate: PDTextField!
    @IBOutlet weak var textFieldOthers: PDTextField!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: PDLabel!
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var tableViewOptions: UITableView!
    @IBOutlet weak var radioButtonParent: RadioButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldOthers.enabled = false
        textFieldDate.inputAccessoryView = toolBar
        textFieldDate.inputView = datePicker
        signatureView.layer.cornerRadius = 3.0
        let dateString = "1 Jan 2016"
        let df = NSDateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.dateFromString(dateString)
        if let unwrappedDate = date {
            datePicker.setDate(unwrappedDate, animated: false)
        }

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChildTreatmentViewController.setDateOnLabel))
        tapGesture.numberOfTapsRequired = 1
        labelDate.addGestureRecognizer(tapGesture)
        
        var patientInfo = "I am unable to accompany my child"
        
        let patientName = patient.fullName//getText("\(patient.firstName) \(patient.lastName)")
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " DOB \(getText(patient.dateOfBirth))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateOfBirth)))
        
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range)
        }
        
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(float: 0)], range: NSMakeRange(0, attributedString.length))
        labelDetails.attributedText = attributedString
        
        // Do any additional setup after loading the view.
    }
    
    func setDateOnLabel() {
        labelDate.text = patient.dateToday
        labelDate.textColor = UIColor.blackColor()
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        self.view.endEditing(true)
        if self.selectedOptions.count == 0 {
            let alert = Extention.alert("PLEASE SELECT ALL THAT APPLY")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if self.selectedOptions.contains("OTHER") && textFieldOthers.isEmpty {
            let alert = Extention.alert("PLEASE TYPE OTHER TREATMENT")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if radioButtonParent.selectedButton == nil {
            let alert = Extention.alert("PLEASE SELECT RELATIONSHIP")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if labelDate.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            if !textFieldDate.isEmpty {
                patient.treatmentDate = textFieldDate.text
            } else {
                patient.treatmentDate = nil
            }
            if !textFieldOthers.isEmpty {
                patient.otherTreatment = textFieldOthers.text
            } else {
                patient.otherTreatment = nil
            }
            patient.selectedOptions = self.selectedOptions
            patient.signature1 = signatureView.signatureImage()
            patient.relationship = radioButtonParent.selectedButton.tag == 1 ? "Parent" : "Guardian"
            let childTreatmentFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kChildTreatmentFormVC") as! ChildTreatmentFormViewController
            childTreatmentFormVC.patient = patient
            self.navigationController?.pushViewController(childTreatmentFormVC, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func toolbarDoneButtonAction(sender: AnyObject) {
        textFieldDate.resignFirstResponder()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        textFieldDate.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
    
    @IBAction func datePickerDateChanged(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        textFieldDate.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
}

extension ChildTreatmentViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let titles = ["TOOTH CLEANING", "SEALANTS", "FILLINGS", "EXTRACTIONS OF PRIMARY TEETH (BABY TEETH)", "EXTRACTION OF PERMANENT TEETH", "ROOT CANAL TREATMENT", "OTHER"]
        let title = titles[indexPath.row]
        if selectedOptions.contains(title) {
            selectedOptions.removeAtIndex(selectedOptions.indexOf(title)!)
        } else {
            selectedOptions.append(title)
        }
        if !selectedOptions.contains(titles[6]) {
            textFieldOthers.text = ""
        }
        tableView.reloadData()
    }
}

extension ChildTreatmentViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellChildTreatment", forIndexPath: indexPath) as! ChildTreatmentTableViewCell
        let titles = ["TOOTH CLEANING", "SEALANTS", "FILLINGS", "EXTRACTIONS OF PRIMARY TEETH (BABY TEETH)", "EXTRACTION OF PERMANENT TEETH", "ROOT CANAL TREATMENT", "OTHER"]
        cell.labelTitle.text = titles[indexPath.row]
        cell.buttonRound.selected = selectedOptions.contains(titles[indexPath.row])
        cell.backgroundColor = UIColor.clearColor()
        cell.contentView.backgroundColor = UIColor.clearColor()
        textFieldOthers.enabled = selectedOptions.contains(titles[6])
        
        return cell
        
    }
}
