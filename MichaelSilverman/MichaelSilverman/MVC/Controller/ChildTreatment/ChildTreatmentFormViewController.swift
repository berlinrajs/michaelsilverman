//
//  ChildTreatmentFormViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Office on 2/23/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ChildTreatmentFormViewController: PDViewController {

    var textRanges : [NSRange]! = [NSRange]()
    

    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelDetails1: UILabel!
    @IBOutlet weak var labelDetails2: UILabel!
    @IBOutlet weak var tableViewOptions: UITableView!
    @IBOutlet weak var labelParentDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewSignature.image = patient.signature1
        labelDate.text = patient.dateToday
        labelParentDetails.text = labelParentDetails.text! + " \(patient.relationship!)."
        // Do any additional setup after loading the view.
        
        var patientInfo = "I am unable to accompany my child"

        let patientName = patient.fullName//getText("\(patient.firstName) \(patient.lastName)")
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " DOB \(getText(patient.dateOfBirth))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateOfBirth)))
        
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range)
        }
        
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(float: 0)], range: NSMakeRange(0, attributedString.length))
        labelDetails1.attributedText = attributedString
        
        let text : String! = patient.treatmentDate == nil ? "N/A" : patient.treatmentDate
        
        let patientReason = "To Michael Silverman Dentistry for the treatment on \(getText(text))"
        let attributedStr = NSMutableAttributedString(string: patientReason)
        attributedStr.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: NSMakeRange(patientReason.characters.count - getText(text).characters.count, getText(text).characters.count))
        attributedStr.addAttributes([NSBaselineOffsetAttributeName: NSNumber(float: 0)], range: NSMakeRange(0, attributedStr.length))
        labelDetails2.attributedText = attributedStr
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}


extension ChildTreatmentFormViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cellChildTreatment", forIndexPath: indexPath) as! ChildTreatmentTableViewCell
        let titles = ["TOOTH CLEANING", "SEALANTS", "FILLINGS", "EXTRACTIONS OF PRIMARY TEETH (BABY TEETH)", "EXTRACTION OF PERMANENT TEETH", "ROOT CANAL TREATMENT", "OTHER"]
//        cell.labelTitle.text = titles[indexPath.row]
        if indexPath.row == 6 &&  patient.selectedOptions.contains(titles[6]) && patient.otherTreatment != nil {
            let text = "OTHER \(getText(patient.otherTreatment!))"
            let attributedStr = NSMutableAttributedString(string: text)
            attributedStr.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: NSMakeRange(text.characters.count - getText(patient.otherTreatment!).characters.count, getText(patient.otherTreatment!).characters.count))
            attributedStr.addAttributes([NSBaselineOffsetAttributeName: NSNumber(float: 0)], range: NSMakeRange(0, attributedStr.length))
            cell.labelTitle.attributedText = attributedStr
        } else {
            let text = titles[indexPath.row]
            let attributedStr = NSMutableAttributedString(string: text)
            attributedStr.addAttributes([NSBaselineOffsetAttributeName: NSNumber(float: 0)], range: NSMakeRange(0, attributedStr.length))
            cell.labelTitle.attributedText = attributedStr
        }
        cell.buttonRound.selected = patient.selectedOptions.contains(titles[indexPath.row])
        cell.backgroundColor = UIColor.clearColor()
        cell.contentView.backgroundColor = UIColor.clearColor()        
        return cell
        
    }
}
