//
//  BracesRemovalStep2ViewController.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/4/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class BracesRemovalStep2ViewController: PDViewController {

    
    @IBOutlet var signatureViews: SignatureView!
    @IBOutlet var labelDates: PDLabel!
    @IBOutlet weak var labelPatientOrGuardian : UILabel!
    @IBOutlet weak var labelPatientName : UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        for labelDate in labelDates {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(setDateOnLabel(_:)))
            tapGesture.numberOfTapsRequired = 1
            labelDates.addGestureRecognizer(tapGesture)
//        }
        if is18YearsOld {
            labelPatientOrGuardian.text = "Signature of Patient"
            labelPatientName.text = patient.fullName
        }else{
            labelPatientOrGuardian.text = "Signature of Parent or Guardian"
            labelPatientName.text = ""

        }
    }
    
    var is18YearsOld: Bool {
        return patient.dateOfBirth.is18YearsOld
    }

    func setDateOnLabel(sender : UITapGestureRecognizer) {
        let labelDate = sender.view as! PDLabel
        labelDate.text = patient.dateToday
        labelDate.textColor = UIColor.blackColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        if !signatureViews.isSigned()  {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if labelDates.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.signature5 = signatureViews.signatureImage()
            let bracesRemovalFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kBracesRemovalFormVC") as! BracesRemovalFormViewController
            bracesRemovalFormVC.patient = patient
            self.navigationController?.pushViewController(bracesRemovalFormVC, animated: true)
        }
    }
    
    @IBAction func onBackButtonPressed (){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
//    func findEmptySignature() -> SignatureView? {
//        for signatureView in signatureViews {
//            if !signatureView.isSigned() {
//                return signatureView
//            }
//        }
//        return nil
//    }
//    
//    func findEmptyDate() -> PDLabel? {
//        for labelDate in labelDates {
//            if labelDate.text == "Tap to date" {
//                return labelDate
//            }
//        }
//        return nil
//    }

}
