//
//  BracesRemovalStep1ViewController.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/4/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class BracesRemovalStep1ViewController: PDViewController {

    
    @IBOutlet var signatureViews: [SignatureView]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        if let _ = findEmptySignature()  {
            let alert = Extention.alert("PLEASE FILL ALL INITIALS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.signature1 = signatureViews[0].signatureImage()
            patient.signature2 = signatureViews[1].signatureImage()
            patient.signature3 = signatureViews[2].signatureImage()
            patient.signature4 = signatureViews[3].signatureImage()
            let bracesRemovalStep2VC = self.storyboard?.instantiateViewControllerWithIdentifier("kBracesRemovalStep2VC") as! BracesRemovalStep2ViewController
            bracesRemovalStep2VC.patient = patient
            self.navigationController?.pushViewController(bracesRemovalStep2VC, animated: true)
        }
    }
    
    
    func findEmptySignature() -> SignatureView? {
        for signatureView in signatureViews {
            if !signatureView.isSigned() {
                return signatureView
            }
        }
        return nil
    }

}