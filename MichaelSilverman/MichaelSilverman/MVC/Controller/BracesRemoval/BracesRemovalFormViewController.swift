//
//  BracesRemovalFormViewController.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/4/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class BracesRemovalFormViewController: PDViewController {

    
    @IBOutlet weak var imageViewInitial1: UIImageView!
    @IBOutlet weak var imageViewInitial2: UIImageView!
    @IBOutlet weak var imageViewInitial3: UIImageView!
    @IBOutlet weak var imageViewInitial4: UIImageView!

    @IBOutlet weak var imageViewSignature1: UIImageView!
    @IBOutlet weak var imageViewSignature2: UIImageView!

    @IBOutlet var labelDates: [UILabel]!
    @IBOutlet weak var labelName: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageViewInitial1.image = patient.signature1
        imageViewInitial2.image = patient.signature2
        imageViewInitial3.image = patient.signature3
        imageViewInitial4.image = patient.signature4
        if is18YearsOld{
            imageViewSignature1.image = patient.signature5
            labelDates[0].text = patient.dateToday

        }else{
            imageViewSignature2.image = patient.signature5
            labelDates[1].text = patient.dateToday


        }
        
//        for labelDate in labelDates {
//            labelDate.text = patient.dateToday
//        }
        
        let patientName = patient.fullName//"\(patient.firstName) \(patient.lastName)"

        labelName.text = patientName
        // Do any additional setup after loading the view.
    }

    var is18YearsOld: Bool {
        return patient.dateOfBirth.is18YearsOld
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
