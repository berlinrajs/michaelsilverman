//
//  PDViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

let mainStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
let consentStoryboard : UIStoryboard = UIStoryboard(name: "Consent", bundle: NSBundle.mainBundle())
let newConsentStoryboard : UIStoryboard = UIStoryboard(name: "NewConsent", bundle: NSBundle.mainBundle())

var patientName : String!

class PDViewController: UIViewController {
    
    @IBOutlet var buttonSubmit: PDButton?
    @IBOutlet var buttonBack: PDButton?
    
    @IBOutlet var pdfView: UIScrollView?
    
    var patient: PDPatient!
    var isFromPreviousForm: Bool {
        get {
            return navigationController?.viewControllers.count > 3 ? true : false
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = .None
        buttonBack?.hidden = isFromPreviousForm && buttonSubmit == nil
        buttonSubmit?.backgroundColor = UIColor.greenColor()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonBackAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onSubmitButtonPressed (sender : UIButton){
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "MICHAEL SILVERMAN", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
            let alertOkAction = UIAlertAction(title: "SETTINGS", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        }
        let pdfManager = PDFManager()
        pdfManager.authorizeDrive(self.view) { (success) -> Void in
            if success {
                self.buttonSubmit?.hidden = true
                self.buttonBack?.hidden = true
                if self.pdfView != nil {
                    pdfManager.createPDFForScrollView(self.pdfView!, fileName: self.patient.selectedForms.first!.formTitle!.fileName, patient: self.patient, completionBlock: { (finished) -> Void in
                        if finished {
                            self.gotoNextForm(false)
                        } else {
                            self.buttonSubmit?.hidden = false
                            self.buttonBack?.hidden = false
                        }
                    })
                } else {
                    pdfManager.createPDFForView(self.view, fileName: self.patient.selectedForms.first!.formTitle!.fileName, patient: self.patient, completionBlock: { (finished) -> Void in
                        if finished {
                            self.gotoNextForm(false)
                        } else {
                            self.buttonSubmit?.hidden = false
                            self.buttonBack?.hidden = false
                        }
                    })
                }
            } else {
                self.buttonSubmit?.hidden = false
                self.buttonBack?.hidden = false
            }
        }
    }
    
    
    func gotoNextForm(showBackButton : Bool) {
        if isFromPreviousForm {
            if patient.selectedForms.count > 0 { patient.selectedForms.removeFirst() }
        }
        let formNames = (patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
        if formNames.contains(kNewPatientSignInForm){
            let parentInfoVC = mainStoryBoard.instantiateViewControllerWithIdentifier("kParentInfoVC") as! ParentInfoViewController
            parentInfoVC.patient = patient
            self.navigationController?.pushViewController(parentInfoVC, animated: true)
        }else if formNames.contains(kMedicalHistoryForm){
            let medicalHistoryStep1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kMedicalHistoryStep1VC") as! MedicalHistoryStep1ViewController
            medicalHistoryStep1VC.patient = patient
            self.navigationController?.pushViewController(medicalHistoryStep1VC, animated: true)
        } else if formNames.contains(kPatientAuthorization) {
            let patientAuthorizationVC = mainStoryBoard.instantiateViewControllerWithIdentifier("kPatientAuthorizationVC") as! PatientAuthorizationViewController
            patientAuthorizationVC.patient = patient
            self.navigationController?.pushViewController(patientAuthorizationVC, animated: true)
        }else if formNames.contains(kInsuranceCard) {
            let cardCapture = mainStoryBoard.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            self.navigationController?.pushViewController(cardCapture, animated: true)
        } else if formNames.contains(kDrivingLicense) {
            let cardCapture = mainStoryBoard.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            cardCapture.isDrivingLicense = true
            self.navigationController?.pushViewController(cardCapture, animated: true)
        } else if formNames.contains(kDentalImplants) {
            let dentalImplantVC = consentStoryboard.instantiateViewControllerWithIdentifier("kDentalImplantsVC") as! DentalImplantsViewController
            dentalImplantVC.patient = patient
            self.navigationController?.pushViewController(dentalImplantVC, animated: true)
        } else if formNames.contains(kToothExtraction) {
            let toothExtractionVC = consentStoryboard.instantiateViewControllerWithIdentifier("kToothExtractionVC") as! ToothExtractionViewController
            toothExtractionVC.patient = patient
            self.navigationController?.pushViewController(toothExtractionVC, animated: true)
        } else if formNames.contains(kPartialDenture) {
            let dentureAdjustmentVC = consentStoryboard.instantiateViewControllerWithIdentifier("kDentureAdjustmentVC") as! DentureAdjustmentViewController
            dentureAdjustmentVC.patient = patient
            self.navigationController?.pushViewController(dentureAdjustmentVC, animated: true)
        } else if formNames.contains(kInofficeWhitening) {
            let inOfficeWhiteningVC = consentStoryboard.instantiateViewControllerWithIdentifier("kInOfficeWhiteningVC") as! InOfficeWhiteningViewController
            inOfficeWhiteningVC.patient = patient
            self.navigationController?.pushViewController(inOfficeWhiteningVC, animated: true)
        } else if formNames.contains(kTreatmentOfChild) {
            let childTreatmentVC = consentStoryboard.instantiateViewControllerWithIdentifier("kChildTreatmentVC") as! ChildTreatmentViewController
            childTreatmentVC.patient = patient
            self.navigationController?.pushViewController(childTreatmentVC, animated: true)
        } else if formNames.contains(kEndodonticTreatment) {
            let endodonticTreatmentVC = consentStoryboard.instantiateViewControllerWithIdentifier("kEndodonticTreatmentVC") as! EndodonticTreatmentViewController
            endodonticTreatmentVC.patient = patient
            self.navigationController?.pushViewController(endodonticTreatmentVC, animated: true)
        } else if formNames.contains(kCrownAndBridges) {
            let crownAndBridgesStep1VC = consentStoryboard.instantiateViewControllerWithIdentifier("CrownsAndBridgesViewControllerScene") as! CrownsAndBridgesViewControllerScene
            crownAndBridgesStep1VC.patient = self.patient
            self.navigationController?.pushViewController(crownAndBridgesStep1VC, animated: true)
        } else if formNames.contains(kOpiodForm) {
            let opiodVC = consentStoryboard.instantiateViewControllerWithIdentifier("kOpiodViewController") as! OpiodViewController
            opiodVC.patient = self.patient
            self.navigationController?.pushViewController(opiodVC, animated: true)
        }else if formNames.contains(kGeneralConsent) {
            let generalVC = consentStoryboard.instantiateViewControllerWithIdentifier("kGeneralConsentVC") as! GeneralConsentViewController
            generalVC.patient = self.patient
            self.navigationController?.pushViewController(generalVC, animated: true)
        } else if formNames.contains(kBracesRemoval) {
            let bracesRemovalStep1VC = newConsentStoryboard.instantiateViewControllerWithIdentifier("kBracesRemovalStep1VC") as! BracesRemovalStep1ViewController
            bracesRemovalStep1VC.patient = self.patient
            self.navigationController?.pushViewController(bracesRemovalStep1VC, animated: true)
        } else if formNames.contains(kTreatmentFillings) {
            let consentFillingsVC = newConsentStoryboard.instantiateViewControllerWithIdentifier("kFinancialPolicyVC") as! FinancialPolicyViewController
            consentFillingsVC.patient = patient
            self.navigationController?.pushViewController(consentFillingsVC, animated: true)
        } else if formNames.contains(kPhotographyRelease) {
//            let parentInfoVC = newConsentStoryboard.instantiateViewControllerWithIdentifier("kParentInfoVC") as! ParentViewController
//            parentInfoVC.patient = patient
//            self.navigationController?.pushViewController(parentInfoVC, animated: true)
            let photographyReleaseVC = newConsentStoryboard.instantiateViewControllerWithIdentifier("kPhotographyReleaseVC") as! PhotographyReleaseViewController
            photographyReleaseVC.patient = self.patient
            self.navigationController?.pushViewController(photographyReleaseVC, animated: true)

        } else if formNames.contains(kGumDisease) {
            let step1VC = newConsentStoryboard.instantiateViewControllerWithIdentifier("kGumDiseaseStep1VC") as! GumDiseaseStep1VC
            step1VC.patient = patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        } else if formNames.contains(kOralSurgery) {
            let oralSurgeryVC = newConsentStoryboard.instantiateViewControllerWithIdentifier("kOralSurgeryVC") as! OralSurgeryViewController
            oralSurgeryVC.patient = self.patient
            self.navigationController?.pushViewController(oralSurgeryVC, animated: true)
        } else if formNames.contains(kOrthodonticTreatment) {
            let orthodonticTreatmentVC = newConsentStoryboard.instantiateViewControllerWithIdentifier("ConsentToOrthodonticTreatmentViewControllerScene") as! ConsentToOrthodonticTreatmentViewControllerScene
            orthodonticTreatmentVC.patient = patient
            self.navigationController?.pushViewController(orthodonticTreatmentVC, animated: true)
        } else if formNames.contains(kPatientHippa) {
            let patientHippaVC = newConsentStoryboard.instantiateViewControllerWithIdentifier("kPaientHippaVC") as! PatientHippaViewController
            patientHippaVC.patient = patient
            self.navigationController?.pushViewController(patientHippaVC, animated: true)

        } else if formNames.contains(kInsurance) {
            let insuranceReleaseVC = newConsentStoryboard.instantiateViewControllerWithIdentifier("kInsuranceReleaseVC") as! InsuranceReleaseViewController
            insuranceReleaseVC.patient = patient
            self.navigationController?.pushViewController(insuranceReleaseVC, animated: true)
        }
        else {
            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
            self.navigationController?.popToRootViewControllerAnimated(true)
       }
    }
}
