//
//  GumDiseaseStep1VC.swift
//  TotalHealthDental
//
//  Created by SRS Web Solutions on 07/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class GumDiseaseStep1VC: PDViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var textFieldDate: PDTextField!
    @IBOutlet weak var radioPeriodontics: RadioButton!
    @IBOutlet weak var radioPockets: RadioButton!
    @IBOutlet weak var textFieldPockets: PDTextField!
    @IBOutlet weak var radioAgreement: RadioButton!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    
    @IBOutlet weak var viewPocketsPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        buttonBack?.hidden = isFromPreviousForm
        labelDate.todayDate = patient.dateToday
        DateInputView.addDatePicker(textFieldDate, selectedDate: "01 JAN 2016")
        labelName.text = patient.fullName
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonNextAction(sender: AnyObject) {
        if textFieldDate.isEmpty {
            let alert = Extention.alert("PLEASE SELECT A DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if radioPeriodontics.selectedButton == nil {
            let alert = Extention.alert("PLEASE ENTER ALL THE DETAILS NEEDED")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if radioPockets.selectedButton == nil {
            let alert = Extention.alert("PLEASE ENTER ALL THE DETAILS NEEDED")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if radioAgreement.selectedButton == nil {
            let alert = Extention.alert("PLEASE CONFIRM THE AGREEMENT")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.periodontalDate = textFieldDate.text!
            patient.periodontalType = radioPeriodontics.selectedButton.tag
            patient.pocketsSelected = radioPockets.selectedButton.tag
            patient.agreementSelected = radioAgreement.selectedButton.tag
            patient.signature1 = signatureView.signatureImage()
            
            let formVC = storyboard?.instantiateViewControllerWithIdentifier("kGumDiseaseFormVC") as! GumDiseaseFormVC
            formVC.patient = patient
            navigationController?.pushViewController(formVC, animated: true)
        }
    }
    
    @IBAction func pockets8mmSelected() {
        showPopup()
    }
    
    func showPopup() {
        textFieldPockets.text = ""
        self.viewPocketsPopup.frame = CGRectMake(0, 0, 512.0, 250.0)
        self.viewPocketsPopup.center = view.center
        self.viewShadow.addSubview(self.viewPocketsPopup)
        self.viewPocketsPopup.transform = CGAffineTransformMakeScale(0.1, 0.1)
        self.viewShadow.hidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewPocketsPopup.transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionDone() {
        textFieldPockets.resignFirstResponder()
        
        self.viewPocketsPopup.removeFromSuperview()
        self.viewShadow.hidden = true
        
        if textFieldPockets.isEmpty {
            radioPockets.deselectAllButtons()
            patient.teethNumbers = ""
        } else {
            patient.teethNumbers = textFieldPockets.text!
        }
    }
}
extension GumDiseaseStep1VC : UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if string.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0 {
            return true
        }
        if textField == textFieldPockets {
            return textField.formatToothNumber(range, string: string)
        }
        return true
    }
}
