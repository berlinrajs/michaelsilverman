//
//  PatientHippaViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 3/2/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientHippaViewController: PDViewController {

    
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var textFieldRelationship: PDTextField!

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        signatureView.layer.cornerRadius = 3.0
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PatientHippaViewController.setDateOnLabel))
        tapGesture.numberOfTapsRequired = 1
        labelDate.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    func setDateOnLabel() {
        labelDate.text = patient.dateToday
        labelDate.textColor = UIColor.blackColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if labelDate.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.signature1 = signatureView.signatureImage()
            if !textFieldRelationship.isEmpty {
                patient.relationship = textFieldRelationship.text
            } else {
                patient.relationship = nil
            }
            let patientHippaFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientHippaFormVC") as! PatientHippaFormViewController
            patientHippaFormVC.patient = patient
            self.navigationController?.pushViewController(patientHippaFormVC, animated: true)
        }
    }

}


extension PatientHippaViewController : UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}