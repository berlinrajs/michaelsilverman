//
//  PatientHippaFormViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 3/2/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientHippaFormViewController: PDViewController {

    var textRanges : [NSRange]! = [NSRange]()

    
    @IBOutlet var labelText1: UILabel!
    @IBOutlet var labelText2: UILabel!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet var viewSignature: UIView!
    @IBOutlet weak var labelRelationship: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imageViewSignature.image = patient.signature1
        if let relation = patient.relationship {
            labelRelationship.text = relation
        }
        
        var patientInfo = "I Patient name"
        
        let patientName = patient.fullName//getText("\(patient.firstName) \(patient.lastName)")
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " date \(getText(patient.dateToday))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateToday)))
        patientInfo = patientInfo + "do hereby consent and acknowledge my agreement to the terms act forth in the HIPAA INFORMATION FORM and any subsequent changes in office policy. I understand that this consent shall remain in force from this time forward."
        
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range)
        }
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(float: 0)], range: NSMakeRange(0, attributedString.length))
        labelText2.attributedText = attributedString
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        scrollView.contentSize = CGSizeMake(screenSize.width, screenSize.height * 2)
        
        
        let labelFrame = CGRectMake(60.0, screenSize.height + 60.0, screenSize.width - 120.0, 200.0)
        labelText1.frame = labelFrame
        scrollView.addSubview(labelText1)
        
        var viewSignatureFrame = self.viewSignature.frame
        viewSignatureFrame.origin.y = CGRectGetMaxY(labelFrame) + 50//(screenSize.height * 2) - 210
        viewSignatureFrame.origin.x = 60.0
        viewSignatureFrame.size.width = screenSize.width - 120.0
        viewSignature.frame = viewSignatureFrame
        self.scrollView.addSubview(viewSignature)

        
    }
    

}
