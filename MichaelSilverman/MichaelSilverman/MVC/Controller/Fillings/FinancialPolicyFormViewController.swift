//
//  FinancialPolicyFormViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class FinancialPolicyFormViewController: PDViewController {

    @IBOutlet var labelDate: [UILabel]!
    @IBOutlet weak var imageViewSignature1: UIImageView!
    @IBOutlet weak var imageViewSignature2: UIImageView!
    @IBOutlet weak var imageViewSignature3: UIImageView!
    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelDOB: UILabel!
    @IBOutlet weak var labelToday: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName//"\(patient.firstName) \(patient.lastName)"
        labelToday.text = patient.dateToday
        labelDOB.text = patient.dateOfBirth
        imageViewSignature1.image = patient.signature1
        imageViewSignature2.image = patient.signature2
        imageViewSignature3.image = patient.signature3
        for label in labelDate {
            label.text = patient.dateToday
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension FinancialPolicyFormViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let serviceObj = patient.consentFillings[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("cellInitials") as! InitialsTableViewCell
        let text = serviceObj.serviceDescription
        let height = text.heightWithConstrainedWidth(553.0 * screenSize.width/768.0, font: cell.labelDetails.font) + 4
        return height
    }
    
}


extension FinancialPolicyFormViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.consentFillings.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellInitials", forIndexPath:  indexPath) as! InitialsTableViewCell
        let serviceObj = patient.consentFillings[indexPath.row]
        cell.configureCell(serviceObj)
        cell.signatureView.tag = indexPath.row
        cell.signatureView.previousImage = serviceObj.signatureView.imageObj
        cell.backgroundColor = UIColor.clearColor()
        cell.contentView.backgroundColor = UIColor.clearColor()
        return cell
    }
    
}