//
//  FinancialPolicyViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class FinancialPolicyViewController: PDViewController {
    
    
    
    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var signatureView2: SignatureView!
    @IBOutlet weak var signatureView3: SignatureView!

    @IBOutlet var labelDate: [DateLabel]!
    @IBOutlet weak var tableViewServices: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraintViewHeight: NSLayoutConstraint!
    var viewHeight : CGFloat!

    var selectedServices : [Service]! = [Service]()

    override func viewDidLoad() {
        super.viewDidLoad()
        for date in labelDate {
            date.todayDate = patient.dateToday
        }
        selectedServices = Service.getAllServices()
        tableViewServices.reloadData()
        scrollView.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        // Do any additional setup after loading the view.
    }
    

    
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        if let _ = findEmptySignature() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if let _ = findEmptyDate() {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.consentFillings = self.selectedServices
            patient.signature1 = signatureView1.signatureImage()
            patient.signature2 = signatureView2.signatureImage()
            patient.signature3 = signatureView3.signatureImage()
            let financialPolicyFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kFinancialPolicyFormVC") as! FinancialPolicyFormViewController
            financialPolicyFormVC.patient = patient
            self.navigationController?.pushViewController(financialPolicyFormVC, animated: true)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if viewHeight == nil {
            let height = (screenSize.height - tableViewServices.frame.height) + tableViewServices.contentSize.height
            constraintViewHeight.constant = height
            scrollView.contentSize = CGSizeMake(screenSize.width, height)
            scrollView.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
            viewHeight = height
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func findEmptyDate() -> DateLabel? {
        for date in labelDate {
            if !date.dateTapped  {
                return date
            }
        }
        return nil
    }
    
    func findEmptySignature() -> SignatureView? {
        for signatureView in [signatureView1, signatureView2, signatureView3] {
            if !signatureView.isSigned()  {
                return signatureView
            }
        }
        return nil
    }
}


extension FinancialPolicyViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let serviceObj = selectedServices[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("cellInitials") as! InitialsTableViewCell
        let text = serviceObj.serviceDescription
        let height = text.heightWithConstrainedWidth(568.0 * screenSize.width/768.0, font: cell.labelDetails.font) + 260
        return height
    }
}


extension FinancialPolicyViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedServices.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellInitials", forIndexPath:  indexPath) as! InitialsTableViewCell
        let serviceObj = selectedServices[indexPath.row]
        cell.configureCell(serviceObj)
        cell.signatureView.tag = indexPath.row
        cell.signatureView.previousImage = serviceObj.signatureView.imageObj
        cell.signatureView.delegate = self
        cell.backgroundColor = UIColor.clearColor()
        cell.contentView.backgroundColor = UIColor.clearColor()
        return cell
    }
}

extension FinancialPolicyViewController : SignatureViewDelegate {
    func drawingCompleted(image: UIImage!, signView signatureView: SignatureView!) {
        let serviceObj = selectedServices[signatureView.tag]
        serviceObj.signatureView.imageObj = image
        selectedServices[signatureView.tag] = serviceObj
    }
}
