//
//  HomeViewController.swift
//  AdultMedicalForm
//
//  Created by SRS Web Solutions on 06/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HomeViewController: PDViewController {
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var tablevViewForm: UITableView!
    @IBOutlet weak var textFieldToothNumbers: PDTextField!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet var viewToothNumbers: PDView!
    @IBOutlet weak var viewAlert: PDView!
    @IBOutlet weak var labelVersion: UILabel!
    var privacyAcknowledgementTag : Int = 0
    var privacyAcknowledgementReason : String = ""
    var consentIndex : Int = 5
    var selectedForms : [Forms]! = [Forms]()
    var formList : [Forms]! = [Forms]()
    var okPressed : Bool = false
    
    @IBOutlet weak var labelAlertHeader: UILabel!
    @IBOutlet weak var labelAlertFooter: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let text = NSBundle.mainBundle().infoDictionary?[kCFBundleVersionKey as String] as? String {
            labelVersion.text = text
        }
        self.navigationController?.navigationBar.hidden = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeViewController.showAlert), name: kFormsCompletedNotification, object: nil)
                NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(dateChangedNotification), name: kDateChangedNotification, object: nil)
                self.dateChangedNotification()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        Forms.getAllForms { (isConnectionFailed, forms) -> Void in
            self.formList = forms
            self.tablevViewForm.reloadData()
            if isConnectionFailed == true {
                let alertController = UIAlertController(title: "Michael Silverman", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
                let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.sharedApplication().openURL(url)
                    }
                }
                let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                    
                }
                alertController.addAction(alertOkAction)
                alertController.addAction(alertCancelAction)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func dateChangedNotification() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        self.lblDate.text = dateFormatter.stringFromDate(NSDate()).uppercaseString
    }
    
    @IBAction func buttonActionDone(sender: AnyObject) {
        textFieldToothNumbers.resignFirstResponder()
        self.viewToothNumbers.removeFromSuperview()
        self.viewShadow.hidden = true
        let form =  textFieldToothNumbers.tag < 6 ? formList[textFieldToothNumbers.tag] : formList[5].subForms[textFieldToothNumbers.tag - 6]
        if !textFieldToothNumbers.isEmpty || textFieldToothNumbers.tag == 13 {
            form.isSelected = true
            form.toothNumbers = textFieldToothNumbers.text
        } else {
            form.isSelected = false
        }
        self.tablevViewForm.reloadData()
        
    }
    
    func showAlert() {
        viewAlert.hidden = false
        viewShadow.hidden = false
    }
    
    @IBAction func buttonActionOk(sender: AnyObject) {
        viewAlert.hidden = true
        viewShadow.hidden = true
    }
    
    @IBAction func btnNextAction(sender: AnyObject) {
        selectedForms.removeAll()
        for (_, form) in formList.enumerate() {
            if form.isSelected == true {
                if form.formTitle == kConsentForms  {
                    for subForm in form.subForms {
                        if subForm.isSelected == true {
                            selectedForms.append(subForm)
                        }
                    }
                } else {
                    selectedForms.append(form)
                }
            }
        }
        self.view.endEditing(true)
        if selectedForms.count > 0 {
            selectedForms.sortInPlace({ (formObj1, formObj2) -> Bool in
                return formObj1.index < formObj2.index
            })
            let patient = PDPatient(forms: selectedForms)
            patient.dateToday = lblDate.text
            let patientInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientInfoVC") as! PatientInfoViewController
//            patient.privacyAcknowledgementTag = privacyAcknowledgementTag
//            patient.privacyAcknowledgementReason = privacyAcknowledgementReason
//            patient.okPressed = okPressed
            patientInfoVC.patient = patient
            self.navigationController?.pushViewController(patientInfoVC, animated: true)
        } else {
            let alert = Extention.alert("PLEASE SELECT ANY FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showPopup() {
        textFieldToothNumbers.text = ""
        self.viewToothNumbers.frame = CGRectMake(0, 0, 512.0, 250.0)
        self.viewToothNumbers.center = self.view.center
        self.viewShadow.addSubview(self.viewToothNumbers)
        self.viewToothNumbers.transform = CGAffineTransformMakeScale(0.1, 0.1)
        self.viewShadow.hidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewToothNumbers.transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
}
extension HomeViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 5 {
            let subForms = formList[5].subForms
            for subFrom in subForms {
                subFrom.isSelected = false
            }
            let form = self.formList[5]
            form.isSelected = !form.isSelected
            var indexPaths : [NSIndexPath] = [NSIndexPath]()
            for (idx, _) in form.subForms.enumerate() {
                let indexPath = NSIndexPath(forRow: 6 + idx, inSection: 0)
                indexPaths.append(indexPath)
            }
            if form.isSelected == true {
                tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Bottom)
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    if form.isSelected == true {
                        tableView.scrollToRowAtIndexPath(indexPaths.last!, atScrollPosition: .Bottom, animated: true)
                    }
                }
            } else {
                
                tableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: .Bottom)
            }
            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
            return
        }
        // let formsCount = formList[5].isSelected == true ? formList.count + formList[5].subForms.count  : formList.count
        var form : Forms!
        if (indexPath.row < 6) {
            form = formList[indexPath.row]
        } else {
            form = formList[5].subForms[indexPath.row - 6]
            
        }
        
        if form.isToothNumberRequired == true && !form.isSelected {
            textFieldToothNumbers.tag = indexPath.row
            if indexPath.row == 13 {
                textFieldToothNumbers.placeholder = "PLEASE TYPE"
                labelAlertHeader.text = "ENTER PRESCRIPTION"
                labelAlertFooter.text = "If Any"
                textFieldToothNumbers.delegate = nil
                textFieldToothNumbers.keyboardType = UIKeyboardType.Default
            } else if indexPath.row == 11 {
                textFieldToothNumbers.placeholder = "01, 02, 15, 18"
                labelAlertHeader.text = "PLEASE ENTER TOOTH NUMBERS"
                labelAlertFooter.text = "Note: Separate with commas"
                textFieldToothNumbers.delegate = self
                textFieldToothNumbers.keyboardType = UIKeyboardType.NumberPad
            } else {
                textFieldToothNumbers.placeholder = "01, 02, 15, 18"
                labelAlertHeader.text = "PLEASE ENTER TOOTH NUMBERS"
                labelAlertFooter.text = "Note: Separate with commas"
                textFieldToothNumbers.delegate = self
                textFieldToothNumbers.keyboardType = UIKeyboardType.NumberPad
            }
            self.showPopup()
        } else {
            form.isSelected = !form.isSelected
        }
        tableView.reloadData()
        
    }
}


//extension HomeViewController : UITableViewDelegate {
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        if indexPath.row == consentIndex {
//            let subForms = formList[consentIndex].subForms
//            for subFrom in subForms {
//                subFrom.isSelected = false
//            }
//            let form = self.formList[consentIndex]
//            form.isSelected = !form.isSelected
//            var indexPaths : [NSIndexPath] = [NSIndexPath]()
//            for (idx, _) in form.subForms.enumerate() {
//                let indexPath = NSIndexPath(forRow: (consentIndex + 1) + idx, inSection: 0)
//                indexPaths.append(indexPath)
//            }
//            if form.isSelected == true {
//                tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Bottom)
//                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * Double(NSEC_PER_SEC)))
//                dispatch_after(delayTime, dispatch_get_main_queue()) {
//                    if form.isSelected == true {
//                        tableView.scrollToRowAtIndexPath(indexPaths.last!, atScrollPosition: .Bottom, animated: true)
//                    }
//                }
//            } else {
//                
//                tableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: .Bottom)
//            }
//            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
//            return
//
//        }
//        
//        var form : Forms!
//        if (indexPath.row <= consentIndex) {
//            form = formList[indexPath.row]
//            form.isSelected = !form.isSelected!
//            tableView.reloadData()
//        } else {
//            form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
//            form.isSelected = !form.isSelected!
//            tableView.reloadData()
//        }
//        if form.isToothNumberRequired == true && form.isSelected == true && indexPath.row == consentIndex + 5{
//                            textFieldToothNumbers.placeholder = "DATE"
//                            labelAlertHeader.text = "PLEASE ENTER EXECUTION & EFFECTIVE DATE"
//                            labelAlertFooter.text = ""
//                            textFieldToothNumbers.delegate = self
//                            textFieldToothNumbers.tag = indexPath.row
//                            //textFieldToothNumbers.keyboardType = UIKeyboardType.NumberPad
//                            DateInputView.addDatePickerForTextField(textFieldToothNumbers)
//                            self.showPopup()
//            
//
//        }
//        
////        if form.isSelected && indexPath.row == 16{
////            PrivacyAlert.sharedInstance.showPopUp(self.view, completion: { (acknowledgementTag, acknowledgementReason, efforts, isOkSelected) in
////                self.privacyAcknowledgementTag = acknowledgementTag
////                self.privacyAcknowledgementReason = acknowledgementReason
////                self.okPressed = isOkSelected
////                }, showAlert: { (alertMessage) in
////                    let alert = Extention.alert(alertMessage)
////                    self.presentViewController(alert, animated: true, completion: nil)
////            })
////        }
////        
////        if form.isToothNumberRequired == true && form.isSelected {
////            textFieldToothNumbers.tag = indexPath.row
////            if indexPath.row == 11 {
////                textFieldToothNumbers.placeholder = "01, 02, 15, 18"
////                labelAlertHeader.text = "PLEASE ENTER TOOTH NUMBERS"
////                labelAlertFooter.text = "Note: Separate with commas"
////                textFieldToothNumbers.delegate = self
////                textFieldToothNumbers.keyboardType = UIKeyboardType.NumberPad
////            } else if indexPath.row == 12 {
////                textFieldToothNumbers.placeholder = "PLEASE TYPE"
////                labelAlertHeader.text = "ENTER PRESCRIPTION"
////                labelAlertFooter.text = "If Any"
////                textFieldToothNumbers.delegate = nil
////                textFieldToothNumbers.keyboardType = UIKeyboardType.Default
////            } else {
////                textFieldToothNumbers.placeholder = "01, 02, 15, 18"
////                labelAlertHeader.text = "PLEASE ENTER TOOTH NUMBERS"
////                labelAlertFooter.text = "Note: Separate with commas"
////                textFieldToothNumbers.delegate = self
////                textFieldToothNumbers.keyboardType = UIKeyboardType.NumberPad
////            }
////            self.showPopup()
////        }
//       // else {
//        //    form.isSelected = !form.isSelected
//        //}
// 
//        
//    }
//}

extension HomeViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if formList.count > 0{
            let subForms = formList[consentIndex].subForms
            return formList[consentIndex].isSelected == true ? formList.count + subForms.count  : formList.count
        } else {
            return 0
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return  44
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if (indexPath.row <= consentIndex) {
            let cell = tableView.dequeueReusableCellWithIdentifier("CellMainForm", forIndexPath: indexPath) as! HomePageTableViewCell
            let form = formList[indexPath.row]
            cell.lblName.text = form.formTitle
            cell.imgViewCheckMark.hidden = !form.isSelected
            cell.backgroundColor = UIColor.clearColor()
            cell.contentView.backgroundColor = UIColor.clearColor()
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("CellSubForm", forIndexPath: indexPath) as! HomePageTableViewCell
            let form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
            cell.lblName.text = form.formTitle
            cell.imgViewCheckMark.hidden = !form.isSelected
            cell.backgroundColor = UIColor.clearColor()
            cell.contentView.backgroundColor = UIColor.clearColor()
            return cell
        }
        
    }
    
}

extension HomeViewController : UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if string.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0 {
            return true
        }
        if string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890,").invertedSet)?.last != nil {
            return false
        }
        
        let newRange = textField.text!.startIndex.advancedBy(range.location)..<textField.text!.startIndex.advancedBy(range.location + range.length)
        let textFieldString = textField.text!.stringByReplacingCharactersInRange(newRange, withString: string)
        let textString = textFieldString.componentsSeparatedByString(",")
        
        if textFieldString.characters.count > 2 {
            let lastString = textFieldString.substringFromIndex(textFieldString.startIndex.advancedBy(textFieldString.characters.count - 1))
            let lastTwoStrings = textFieldString.substringFromIndex(textFieldString.startIndex.advancedBy(textFieldString.characters.count - 2))
            let lastThreeStrings = textFieldString.substringFromIndex(textFieldString.startIndex.advancedBy(textFieldString.characters.count - 3))
            
            if lastTwoStrings == ",," {
                return false
            }
            if lastString == "," && lastThreeStrings.componentsSeparatedByString(",").count == 3 {
                let requiredString = textFieldString.substringToIndex(textFieldString.startIndex.advancedBy(textFieldString.characters.count - 2)) + "0" + lastTwoStrings
                textField.text = requiredString
                return false
            }
            
        } else {
            if textFieldString.characters.count == 2 {
                let lastString = textFieldString.substringFromIndex(textFieldString.startIndex.advancedBy(textFieldString.characters.count - 1))
                if lastString == "," {
                    textField.text = "0" + textFieldString
                    return false
                }
                
            }
            if textFieldString == "," {
                return false
            }
        }
        
        
        
        for text in textString {
            if text == "0" {
                return true
            }
            if text == "00" {
                return false
            }
            if Int(text) > 35 {
                return false
            }
        }
        return true
    }
}



