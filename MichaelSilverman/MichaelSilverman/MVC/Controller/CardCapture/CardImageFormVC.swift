//
//  CardImageFormVC.swift
//  AceDental
//
//  Created by SRS Web Solutions on 29/04/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CardImageFormVC: PDViewController {

    @IBOutlet weak var imageViewFront: ActionImageView!
    @IBOutlet weak var imageViewBack: ActionImageView!
    
//    @IBOutlet weak var buttonBack: UIButton!
//    @IBOutlet weak var buttonSubmit: UIButton!
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelBack: UILabel!
    
    
    
    var isDrivingLicense: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
//        buttonSubmit.backgroundColor = UIColor.greenColor()

        imageViewFront.borderColor = UIColor.lightGrayColor()
        imageViewBack.borderColor = UIColor.lightGrayColor()
        
        labelTitle.text = isDrivingLicense ? "DRIVING LICENSE" : "INSURANCE CARD"
        labelDate.text = "DATE: " + patient.dateToday
        
        labelName.text = "PATIENT NAME: " + patient.fullName
        self.imageViewFront.image = patient.frontImage
        if patient.backImage == nil {
            self.imageViewBack.hidden = true
            self.labelBack.hidden = true
        } else {
            self.imageViewBack.image = patient.backImage
            self.imageViewBack.hidden = false
            self.labelBack.hidden = false
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
