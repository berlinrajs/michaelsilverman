//
//  ToothExtractionFormViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Office on 2/24/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ToothExtractionFormViewController: PDViewController {

    var textRanges : [NSRange]! = [NSRange]()
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageViewSignature1: UIImageView!
    @IBOutlet weak var imageViewSignature2: UIImageView!
    @IBOutlet weak var imageViewSignature3: UIImageView!

    @IBOutlet weak var labelDetails: UILabel!
    
    @IBOutlet var arrayOptionButtons1: [UIButton]!
    @IBOutlet var arrayOptionButtons2: [UIButton]!
    
    @IBOutlet weak var labelOther1: FormLabel!
    @IBOutlet weak var labelOther2: FormLabel!
    
    @IBOutlet weak var prognosisProcedure: FormLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageViewSignature1.image = patient.signature1
        imageViewSignature2.image = patient.signature2
        imageViewSignature3.image = patient.signature3
        
        labelDate.text = patient.dateToday
        
        
        var patientInfo = "Patient Name:"

        let patientName = patient.fullName//getText("\(patient.firstName) \(patient.lastName)")
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " DOB: \(getText(patient.dateOfBirth))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateOfBirth)))
        
        let form = patient.selectedForms.filter { (formObj) -> Bool in
            return formObj.formTitle == kToothExtraction
        }
        
        patientInfo = patientInfo + "\nI request to extract tooth/teeth# \(getText(form[0].toothNumbers))"
        textRanges.append(patientInfo.rangeOfText(getText(form[0].toothNumbers)))

        
        patientInfo = patientInfo + "\nThe doctor has recommended this treatment because of"
        
        if let option1 = patient.toothExtractionQuestions1 {
            let optionArray = option1.capitalizedString.componentsSeparatedByString(", ")
            
            for button in arrayOptionButtons1 {
              

                button.selected = optionArray.contains(button.titleForState(UIControlState.Normal)!.capitalizedString)
            }
        }
        
        if let otherText1 = patient.othersText1 {
            labelOther1.text = otherText1
            arrayOptionButtons1.last?.selected = true
        }
        
        if let prognosis = patient.prognosisProcedure {
            prognosisProcedure.text = prognosis
        } else {
            prognosisProcedure.text = getText("N/A")
        }
        
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range)
        }
        
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(float: 0)], range: NSMakeRange(0, attributedString.length))
        labelDetails.attributedText = attributedString
        
//        patientInfo = patientInfo + "\nExtraction involves the complete removal of a tooth from the mouth. Some extractions require cutting into the gums and removing bone and/or cutting the tooth into sections prior to removal. The intended benefit of this treatment is to relieve my current symptoms and/or permit further planned treatment. The prognosis for this procedure is "
//        
//        let text = getText("N/A")
//        
//        if let prognosis = patient.prognosisProcedure {
//            patientInfo = patientInfo + " \(getText(prognosis))"
//            textRanges.append(patientInfo.rangeOfText(getText(prognosis)))
//        } else {
//            patientInfo = patientInfo + " \(text)"
//            textRanges.append(patientInfo.rangeOfText(text))
//        }
//        patientInfo = patientInfo + "\nI have been informed of the following possible alternative treatments, and the costs risks & benefits of each: "
//        
//        patientInfo = patientInfo + "\n"
        if let option2 = patient.toothExtractionQuestions2 {
            let optionArray = option2.capitalizedString.componentsSeparatedByString(", ")
            
            for button in arrayOptionButtons2 {
                
                button.selected = optionArray.contains(button.titleForState(UIControlState.Normal)!.capitalizedString)
            }
        }
        if let otherText2 = patient.othersText2 {
            labelOther2.text = otherText2
            arrayOptionButtons2.last?.selected = true
        }
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
