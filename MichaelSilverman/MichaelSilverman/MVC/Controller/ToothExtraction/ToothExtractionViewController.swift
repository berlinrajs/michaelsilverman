//
//  ToothExtractionViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Leojin Bose on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ToothExtractionViewController: PDViewController {

    var toothExtractionStep1 : [PDOption]! = [PDOption]()

    
    @IBOutlet weak var textFieldOthers: PDTextField!
    @IBOutlet weak var collectionViewoptions: UICollectionView!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchData()
        // Do any additional setup after loading the view.
    }
    
    func fetchData() {
        PDOption.fetchQuestionsToothExtractionForm1 { (result, success) -> Void in
            if success {
                self.toothExtractionStep1.appendContentsOf(result!)
                self.collectionViewoptions.reloadData()
            }
        }
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        self.view.endEditing(true)
        let selectedOptions = self.toothExtractionStep1.filter { (option) -> Bool in
            return option.isSelected != nil && option.isSelected == true
        }
        
        if selectedOptions.count == 0 && !textFieldOthers.enabled {
            let alert = Extention.alert("PLEASE SELECT ANY OPTION")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if textFieldOthers.enabled && textFieldOthers.isEmpty {
            let alert = Extention.alert("PLEASE ENTER OTHER REASON")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            if !textFieldOthers.isEmpty {
                patient.othersText1 = textFieldOthers.text
            } else {
                patient.othersText1 = nil
            }
            let step1Text : String! = ((selectedOptions as NSArray).valueForKey("question") as! [String]).joinWithSeparator(", ")
            patient.toothExtractionQuestions1 = step1Text.isEmpty ? nil : step1Text
            let toothExtractionStep2VC = self.storyboard?.instantiateViewControllerWithIdentifier("kToothExtractionStep2VC") as! ToothExtractionStep2ViewController
            toothExtractionStep2VC.patient = patient
            self.navigationController?.pushViewController(toothExtractionStep2VC, animated: true)
        }
    }
    
    @IBAction func buttonActionOthers(sender: UIButton) {
        sender.selected = !sender.selected
        textFieldOthers.enabled = sender.selected
        if sender.selected == false {
            textFieldOthers.text = ""
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ToothExtractionViewController : UICollectionViewDelegate {
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let obj = self.toothExtractionStep1[indexPath.item]
        if let selected = obj.isSelected {
            obj.isSelected = !selected
        } else {
            obj.isSelected = true
        }
        collectionView.reloadData()
    }
}

extension ToothExtractionViewController : UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.toothExtractionStep1.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cellMedicalHistory", forIndexPath: indexPath) as! MedicalHistoryCollectionViewCell
        let obj = self.toothExtractionStep1[indexPath.item]
        cell.configureCellOption(obj)
        return cell
    }
}
