//
//  DentureFormViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Leojin Bose on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DentureFormViewController: PDViewController {


    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var labelDate2: UILabel!
    @IBOutlet weak var imageViewSignature1: UIImageView!
    @IBOutlet weak var imageViewSignature2: UIImageView!
    @IBOutlet weak var labelDetails: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewSignature1.image = patient.signature1
        imageViewSignature2.image = patient.signature2
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        // Do any additional setup after loading the view.
        

        let patientName = patient.fullName//getText("\(patient.firstName) \(patient.lastName)")
        let labelString = labelDetails.text?.stringByReplacingOccurrencesOfString("FIRST NAME LAST NAME", withString: patientName)
        let attributedString = NSMutableAttributedString(string: labelString!)
        let range = labelString?.rangeOfString(patientName)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: NSMakeRange(Int("\(range!.startIndex)")!, Int("\(range!.endIndex)")! - 2))

        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(float: 0)], range: NSMakeRange(0, attributedString.length))
        labelDetails.attributedText = attributedString

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
