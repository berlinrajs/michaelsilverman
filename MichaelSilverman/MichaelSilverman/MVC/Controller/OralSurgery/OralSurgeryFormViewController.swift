//
//  OralSurgeryFormViewController.swift
//  AceDental
//
//  Created by Leojin Bose on 3/23/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class OralSurgeryFormViewController: PDViewController {

    
    var textRanges : [NSRange]! = [NSRange]()
    
    
    @IBOutlet weak var labelDetails: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var viewSignature: UIView!
    @IBOutlet var labelDate1: UILabel!
    @IBOutlet var labelDate2: UILabel!
    @IBOutlet var imageViewSignature1: UIImageView!
    @IBOutlet var imageViewSignature2: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewSignature1.image = patient.signature1
        imageViewSignature2.image = patient.signature2
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        // Do any additional setup after loading the view.
        
        let patientName = patient.fullName//patient.initial == nil ? getText("\(patient.lastName) \(patient.firstName)") : getText("\(patient.lastName) \(patient.firstName) \(patient.initial!)")
        
        
        var patientInfo = "Patient name:"
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " I hereby authorize \(getText("Dr. Michael Silverman"))"
        textRanges.append(patientInfo.rangeOfText(getText("Dr. Michael Silverman")))
        
        patientInfo = patientInfo + "  (doctor name) and any associates to perform the following oral surgery procedure:"
        if let procedure = patient.oralSurgeryProcedure {
            patientInfo = patientInfo + " \(getText(procedure))"
            textRanges.append(patientInfo.rangeOfText(getText(procedure)))
        } else {
            let text = getText("N/A")
            patientInfo = patientInfo + " \(getText(text))"
            textRanges.append(patientInfo.rangeOfText(getText(text)))
        }
        
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range)
        }
        let paragraphStyle = NSMutableParagraphStyle()
        //        paragraphStyle.lineSpacing = 10
        paragraphStyle.alignment = NSTextAlignment.Justified
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(float: 0), NSParagraphStyleAttributeName : paragraphStyle], range: NSMakeRange(0, attributedString.length))
        labelDetails.attributedText = attributedString
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        scrollView.contentSize = CGSizeMake(screenSize.width, screenSize.height * 2)
        var frameSize = self.view.frame
        frameSize.origin.y = screenSize.height
        viewSignature.frame = frameSize
        scrollView.addSubview(viewSignature)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
