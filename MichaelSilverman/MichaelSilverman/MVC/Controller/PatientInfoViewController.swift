//
//  PatientInfoViewController.swift
//  ProDental
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientInfoViewController: PDViewController {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    @IBOutlet weak var textFieldInitial: PDTextField!
    @IBOutlet var toolBar: UIToolbar!

    @IBOutlet weak var textfieldMonth : UITextField!
    @IBOutlet weak var textfieldDate : UITextField!
    @IBOutlet weak var textfieldYear : UITextField!
    @IBOutlet weak var pickerMonth : UIPickerView!
    var arrayMonths = ["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"]

    override func viewDidLoad() {
        super.viewDidLoad()

        pickerMonth.dataSource = self
        pickerMonth.delegate = self
        textfieldMonth.inputView = pickerMonth
        textfieldMonth.inputAccessoryView = toolBar


        labelDate.text = patient.dateToday
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(sender : AnyObject) {
        self.view.endEditing(true)
        if textFieldFirstName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER FIRST NAME")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if textFieldLastName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER LAST NAME")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if textfieldDate.isEmpty || textfieldMonth.isEmpty || textfieldYear.isEmpty{
            let alert = Extention.alert("PLEASE ENTER DATE OF BIRTH")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if invalidDateofBirth{
            let alert = Extention.alert("PLEASE ENTER THE VALID DATE OF BIRTH")
            self.presentViewController(alert, animated: true, completion: nil)

        } else {
                patient.firstName = textFieldFirstName.text
                patient.lastName = textFieldLastName.text
                patient.dateOfBirth = textfieldMonth.text! + " " + textfieldDate.text! + ", " + textfieldYear.text!
                patient.middleInitial = textFieldInitial.isEmpty ? "" : textFieldInitial.text!
                patient.doctorName = "Dr. Ahmed Elgazar, D.D.S"
                patientName = patient.fullName
                self.gotoNextForm(true)
            }
    }
    
    var invalidDateofBirth: Bool {
        get {
            if textfieldMonth.isEmpty || textfieldDate.isEmpty || textfieldYear.isEmpty {
                return true
            } else if Int(textfieldDate.text!)! == 0{
                return true
            } else if !textfieldYear.text!.isValidYear {
                return true
            } else {
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd-MMM-yyyy"
                
                let todayDate = dateFormatter.dateFromString(dateFormatter.stringFromDate(NSDate()))
                let currentDate = dateFormatter.dateFromString("\(textfieldDate.text!)-\(textfieldMonth.text!)-\(textfieldYear.text!)")
                
                if todayDate == nil || currentDate == nil {
                    return true
                }
                if todayDate!.timeIntervalSinceDate(currentDate!) < 0 {
                    return true
                }
                return false
            }
        }
    }
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func toolbarDoneButtonAction(sender: AnyObject) {
        textfieldMonth.resignFirstResponder()
        let string1 = arrayMonths[pickerMonth.selectedRowInComponent(0)]
        textfieldMonth.text = string1.substringWithRange(string1.startIndex ..< string1.startIndex.advancedBy(3))
    }
    
}


extension PatientInfoViewController : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldInitial {
            return textField.formatInitial(range, string: string)
        }else if textField == textfieldDate{
            return textField.formatDate(range, string: string)
        }else if textField == textfieldYear{
            return textField.formatNumbers(range, string: string, count: 4)
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension PatientInfoViewController : UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayMonths.count
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayMonths[row]
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let string1 = arrayMonths[row]
        textfieldMonth.text = string1.substringWithRange(string1.startIndex ..< string1.startIndex.advancedBy(3))
    }
}
