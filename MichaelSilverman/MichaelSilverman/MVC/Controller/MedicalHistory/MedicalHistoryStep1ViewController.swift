//
//  MedicalHistoryStep1ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryStep1ViewController: PDViewController {

    var medicalHistoryStep1 : [PDQuestion]! = [PDQuestion]()
    var selectedButton : RadioButton!
    var fetchCompleted : Bool! = false
    
    
    @IBOutlet weak var tableViewQuestions: UITableView!
    @IBOutlet var viewPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textViewAnswer: PDTextView!
    
    @IBOutlet weak var buttonVerified: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchData()
        // Do any additional setup after loading the view.
    }
    
    func fetchData() {
        PDQuestion.fetchQuestionsForm1 { (result, success) -> Void in
            self.fetchCompleted = true
            if success {
                self.medicalHistoryStep1.appendContentsOf(result!)
                self.tableViewQuestions.reloadData()
            }
        }
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        if self.fetchCompleted == true {
//            if let _ = self.findEmptyValue() {
//                let alert = Extention.alert("MISSING INFORMATION.\nPLEASE SELECT YES OR NO")
//                self.presentViewController(alert, animated: true, completion: nil)
//            } else {
            if !buttonVerified.selected {
                let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                self.presentViewController(alert, animated: true, completion: nil)
            } else {
                patient.medicalHistoryQuestions1 = self.medicalHistoryStep1
                let medicalHistoryStep2VC = self.storyboard?.instantiateViewControllerWithIdentifier("kMedicalHistoryStep2VC") as! MedicalHistoryStep2ViewController
                medicalHistoryStep2VC.patient = self.patient
                self.navigationController?.pushViewController(medicalHistoryStep2VC, animated: true)
            }
        }
    }

    @IBAction func buttonVerifiedAction(sender: UIButton) {
        sender.selected = !sender.selected
    }
    @IBAction func buttonActionPopupOk(sender: AnyObject) {
        
        let obj = medicalHistoryStep1[selectedButton.tag]
        if !textViewAnswer.isEmpty && textViewAnswer.text != "IF YES TYPE HERE" {
            obj.answer = textViewAnswer.text
            obj.selectedOption = true
        } else {
            selectedButton.selected = false
            obj.selectedOption = false
        }
        textViewAnswer.resignFirstResponder()
        self.viewPopup.removeFromSuperview()
        self.viewShadow.hidden = true
    }
    
    
    func findEmptyValue() -> PDQuestion? {
        for question in self.medicalHistoryStep1 {
            if question.selectedOption == nil {
                return question
            }
        }
        return nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension MedicalHistoryStep1ViewController : UITextViewDelegate {
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "IF YES TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == "" {
            textView.text = "IF YES TYPE HERE"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension MedicalHistoryStep1ViewController : MedicalHistoryCellDelegate {
    func radioButtonAction(sender: RadioButton) {
        selectedButton = sender
        
        let frameSize = CGRectMake(0, 0, 348, 174)
        self.viewPopup.frame = frameSize
        self.viewPopup.center = self.view.center
        self.viewShadow.addSubview(self.viewPopup)
        self.viewPopup.transform = CGAffineTransformMakeScale(0.1, 0.1)
        textViewAnswer.text = "IF YES TYPE HERE"
        textViewAnswer.textColor = UIColor.lightGrayColor()
        self.viewShadow.hidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewPopup.transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
}


extension MedicalHistoryStep1ViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return medicalHistoryStep1.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellMedicalHistoryStep1", forIndexPath: indexPath) as! MedicalHistoryStep1TableViewCell
        let obj = medicalHistoryStep1[indexPath.row]
        cell.configureCell(obj)
        cell.buttonYes.tag = indexPath.row
        cell.buttonNo.tag = indexPath.row
        cell.delegate = self
        return cell
        
        
    }
}