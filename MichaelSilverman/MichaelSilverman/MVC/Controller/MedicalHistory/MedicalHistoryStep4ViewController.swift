//
//  MedicalHistoryStep4ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryStep4ViewController: PDViewController {

    var medicalHistoryStep4 : [PDOption]! = [PDOption]()
    var currentIndex : Int = 0
    var fetchCompleted : Bool! = false
    
    @IBOutlet weak var buttonNext: PDButton!
    @IBOutlet weak var buttonBack1 : UIButton!
    @IBOutlet weak var tableViewQuestions: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var buttonVerified: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchData()
        // Do any additional setup after loading the view.
    }
    
    func fetchData() {
        PDOption.fetchQuestionsForm4 { (result, success) -> Void in
            self.activityIndicator.stopAnimating()
            self.fetchCompleted = true
            if success {
                self.medicalHistoryStep4.appendContentsOf(result!)
                self.tableViewQuestions.reloadData()
            }
        }
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        if currentIndex == 0 {
            self.navigationController?.popViewControllerAnimated(true)
        } else {
            buttonBack1.userInteractionEnabled = false
            buttonNext.userInteractionEnabled = false
            self.buttonVerified.selected = true
            self.activityIndicator.startAnimating()
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.4 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
                self.currentIndex = self.currentIndex - 1
                self.tableViewQuestions.reloadData()
                self.buttonBack1.userInteractionEnabled = true
                self.buttonNext.userInteractionEnabled = true
            }
        }
    }
    @IBAction func buttonVerifiedAction(sender: UIButton) {
        sender.selected = !sender.selected
    }
    @IBAction func buttonActionNext(sender: AnyObject) {
        if self.fetchCompleted == true {
            if ((currentIndex + 1) * 20) < self.medicalHistoryStep4.count {
//                if let _ = findEmptyValue() {
//                    let alert = Extention.alert("MISSING INFORMATION.\nPLEASE SELECT YES OR NO")
//                    self.presentViewController(alert, animated: true, completion: nil)
//                } else {
                if !buttonVerified.selected {
                    let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                    self.presentViewController(alert, animated: true, completion: nil)
                } else {
                    buttonBack1.userInteractionEnabled = false
                    buttonNext.userInteractionEnabled = false
                    self.buttonVerified.selected = false
                    self.activityIndicator.startAnimating()
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.4 * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        self.activityIndicator.stopAnimating()
                        self.currentIndex = self.currentIndex + 1
                        self.tableViewQuestions.reloadData()
                        self.buttonBack1.userInteractionEnabled = true
                        self.buttonNext.userInteractionEnabled = true
                    }
                }
                
            } else {
                if !buttonVerified.selected {
                    let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                    self.presentViewController(alert, animated: true, completion: nil)
                } else {
                    patient.medicalHistoryQuestions4 = self.medicalHistoryStep4
                    let medicalHistoryStep5VC = self.storyboard?.instantiateViewControllerWithIdentifier("kMedicalHistoryStep5VC") as! MedicalHistoryStep5ViewController
                    medicalHistoryStep5VC.patient = self.patient
                    self.navigationController?.pushViewController(medicalHistoryStep5VC, animated: true)
                }
            }
        }
    }
    
    func findEmptyValue() -> PDOption? {
        let objects = medicalHistoryStep4.filter { (obj) -> Bool in
            return obj.index >= (currentIndex * 20) && obj.index < ((currentIndex + 1) * 20)
        }
        for question in objects {
            if question.isSelected == nil {
                return question
            }
        }
        return nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension MedicalHistoryStep4ViewController : MedicalHistoryCellDelegate {
    func radioButtonAction(sender: RadioButton) {
        self.tableViewQuestions.reloadData()
    }
}


extension MedicalHistoryStep4ViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let objects = medicalHistoryStep4.filter { (obj) -> Bool in
            return obj.index >= (currentIndex * 20) && obj.index < ((currentIndex + 1) * 20)
        }
        return objects.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellMedicalHistoryStep4", forIndexPath: indexPath) as! MedicalHistoryStep1TableViewCell
        let index = (currentIndex * 20) + indexPath.row
        let obj = medicalHistoryStep4[index]
        cell.configureCellOption(obj)
        if let selected = obj.isSelected {
            cell.buttonYes.selected = selected
        } else {
            cell.buttonYes.selected = false
        }
        cell.buttonYes.tag = index
        cell.buttonNo.tag = index
        cell.delegate = self
        return cell
    }
}