//
//  MedicalHistoryStep2ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryStep2ViewController: PDViewController {
    
    var medicalHistoryStep2 : [PDOption]! = [PDOption]()
    var medicalHistoryStep3 : [PDOption]! = [PDOption]()
    
    var fetchCompleted : Bool! = false
    
    @IBOutlet weak var radioButton: RadioButton!
    @IBOutlet weak var tableViewQuestions: UITableView!
    @IBOutlet weak var viewTableBG: PDView!
    
    @IBOutlet weak var radioButtonAlergetic: RadioButton!
    @IBOutlet weak var tableViewAlergetic: UITableView!
    @IBOutlet var viewPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textViewOthers: PDTextView!
    var othersObj : PDOption!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTableBG.borderColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
        self.fetchData()
        // Do any additional setup after loading the view.
    }
    
    func fetchData() {
        PDOption.fetchQuestionsForm2 { (result, success) -> Void in
            if success {
                self.medicalHistoryStep2.appendContentsOf(result!)
                self.tableViewQuestions.reloadData()
            }
        }
        PDOption.fetchQuestionsForm3 { (result, success) -> Void in
            self.fetchCompleted = true
            if success {
                self.medicalHistoryStep3.appendContentsOf(result!)
                let objOthers = PDOption(value: "Others")
                self.medicalHistoryStep3.append(objOthers)
                self.othersObj = objOthers
                self.tableViewAlergetic.reloadData()
            }
        }
    }
    
    func findEmptyValue() -> PDOption? {
        for question in self.medicalHistoryStep2 {
            if question.isSelected == true {
                return question
            }
        }
        return nil
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        if self.fetchCompleted == true {
            func gotoNextView() {
                if patient.controlledSubstancesClicked == nil {
                    let alert = Extention.alert("PLEASE ANSWER ALL QUESTIONS")
                    self.presentViewController(alert, animated: true, completion: nil)
                } else {
                    patient.medicalHistoryQuestions2 = self.medicalHistoryStep2
                    patient.medicalHistoryQuestions3 = self.medicalHistoryStep3
                    let medicalHistoryStep4VC = self.storyboard?.instantiateViewControllerWithIdentifier("kMedicalHistoryStep4VC") as! MedicalHistoryStep4ViewController
                    medicalHistoryStep4VC.patient = self.patient
                    self.navigationController?.pushViewController(medicalHistoryStep4VC, animated: true)
                }
                
            }
            if radioButton.selectedButton.tag == 1 {
                if let _ = findEmptyValue() {
                    gotoNextView()
                } else {
                    let alert = Extention.alert("IF YOU ARE WOMAN PLEASE SELECT ANY OF THE FOLLOWING")
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            } else {
                gotoNextView()
            }
        }
    }
    
    @IBAction func buttonActionWomen(sender: RadioButton) {
        if sender.tag == 2 {
            for obj in medicalHistoryStep2 {
                obj.isSelected = nil
            }
            viewTableBG.borderColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
        } else {
            viewTableBG.borderColor = UIColor.whiteColor()
        }
        tableViewQuestions.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionPopupOk(sender: AnyObject) {
        if textViewOthers.tag == 1
        {
            if !textViewOthers.isEmpty && textViewOthers.text != "IF YES TYPE HERE" {
                patient.othersTextForm3 = textViewOthers.text
                othersObj.isSelected = true
                self.tableViewAlergetic.reloadData()
            }
        } else {
            if !textViewOthers.isEmpty && textViewOthers.text != "IF YES TYPE HERE" {
                patient.controlledSubstances = textViewOthers.text
            } else {
                patient.controlledSubstances = nil
                radioButtonAlergetic.setSelectedWithTag(2)
            }
        }
        textViewOthers.resignFirstResponder()
        self.viewPopup.removeFromSuperview()
        self.viewShadow.hidden = true
    }
    
    @IBAction func radioButtonAction(sender: RadioButton) {
        patient.controlledSubstancesClicked = true
        if sender.tag == 1 {
            showPopup(2)
        }
        else
        {
            patient.controlledSubstances = nil

        }
    }
    
    func showPopup(tag : Int) {
        let frameSize = CGRectMake(0, 0, 348, 174)
        self.viewPopup.frame = frameSize
        self.viewPopup.center = self.view.center
        self.viewShadow.addSubview(self.viewPopup)
        self.viewPopup.transform = CGAffineTransformMakeScale(0.1, 0.1)
        textViewOthers.text = "IF YES TYPE HERE"
        textViewOthers.tag = tag
        textViewOthers.textColor = UIColor.lightGrayColor()
        self.viewShadow.hidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewPopup.transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
}

extension MedicalHistoryStep2ViewController : UITextViewDelegate {
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "IF YES TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == "" {
            textView.text = "IF YES TYPE HERE"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension MedicalHistoryStep2ViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView.tag == 1 {
            if radioButton.selectedButton.tag == 1 {
                if indexPath.row == 3 {
                    medicalHistoryStep2[0].isSelected = false
                    medicalHistoryStep2[1].isSelected = false
                    medicalHistoryStep2[2].isSelected = false
                    
                    let obj = medicalHistoryStep2[indexPath.row]
                    obj.isSelected = obj.isSelected != nil && obj.isSelected == true ? false : true
                } else {
                    medicalHistoryStep2[3].isSelected = false
                    
                    let obj = medicalHistoryStep2[indexPath.row]
                    obj.isSelected = obj.isSelected != nil && obj.isSelected == true ? false : true
                }
            }
        } else {
            let obj = medicalHistoryStep3[indexPath.row]
            if obj.question == "Others" {
                if obj.isSelected == nil || obj.isSelected == false {
                    self.showPopup(1)
                } else {
                    patient.othersTextForm3 = nil
                    obj.isSelected = obj.isSelected != nil && obj.isSelected == true ? false : true
                }
            } else {
                obj.isSelected = obj.isSelected != nil && obj.isSelected == true ? false : true
            }
        }
        tableView.reloadData()
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableView.tag == 1 ? 35.0 : 40.0
    }
}

extension MedicalHistoryStep2ViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView.tag == 1 ? medicalHistoryStep2.count : medicalHistoryStep3.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView.tag == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellMedicalHistoryStep2", forIndexPath: indexPath) as! MedicalHistoryStep2TableViewCell
            let obj = medicalHistoryStep2[indexPath.row]
            cell.buttonCheckbox.selected = obj.isSelected != nil && obj.isSelected == true
            cell.buttonCheckbox.enabled = radioButton.selectedButton.tag == 1
            cell.labelTitle.enabled = radioButton.selectedButton.tag == 1
            cell.configureCell(obj)
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellMedicalHistoryStep3", forIndexPath: indexPath) as! MedicalHistoryStep2TableViewCell
            let obj = medicalHistoryStep3[indexPath.row]
            cell.buttonCheckbox.selected = obj.isSelected != nil && obj.isSelected == true
            cell.configureCell(obj)
            return cell
        }
    }
}