//
//  MedicalHistoryFormViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/29/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryFormViewController: PDViewController {

    
    @IBOutlet weak var constraintTableViewForm1Height: NSLayoutConstraint!
    @IBOutlet weak var constraintTableViewForm4Height: NSLayoutConstraint!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelBirthDate: UILabel!
    @IBOutlet weak var labelDateCreated: UILabel!

    @IBOutlet weak var buttonAlergicOthers: RadioButton!
    @IBOutlet weak var labelAlergicOthers: PDLabel!
    
    @IBOutlet weak var radioButtonSubstances: RadioButton!
    @IBOutlet weak var labelSubstances: PDLabel!
    
    @IBOutlet weak var radioButtonIllness: RadioButton!
    @IBOutlet weak var labelIllness: PDLabel!
    
    @IBOutlet weak var textViewComments: PDTextView!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    
    @IBOutlet var arrayButtons: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        constraintTableViewForm1Height.constant = CGFloat(patient.medicalHistoryQuestions1.count * 26)
        constraintTableViewForm4Height.constant = (19 * 14) + 18

        labelName.text = "Patient Name: \(patient.fullName)"
        labelBirthDate.text = "Birth Date: \(patient.dateOfBirth)"
        labelDateCreated.text = "Date Created: \(patient.dateToday)"
        
        if let alergic = patient.othersTextForm3 {
            buttonAlergicOthers.selected = true
            labelAlergicOthers.text = " " + alergic
        }else{
            buttonAlergicOthers.selected = false
        }
        if let substances = patient.controlledSubstances {
            radioButtonSubstances.selected = true
            labelSubstances.text = " " + substances
        } else if patient.controlledSubstancesClicked != nil {
            radioButtonSubstances.selected = false
        }
        
        if let illness = patient.otherIllness {
            radioButtonIllness.selected = true
            labelIllness.text = " " + illness
        } else {
            radioButtonIllness.selected = false
        }
        
        if let comment = patient.comments {
            textViewComments.text = comment
        }
        
        imageViewSignature.image = patient.signature1
        labelDate.text = patient.dateToday
        
        for (idx, buttonQuestion) in self.arrayButtons.enumerate() {
            let obj = patient.medicalHistoryQuestions2[idx]
            buttonQuestion.setTitle(" " + obj.question, forState: .Normal)
            if let selected = obj.isSelected {
                buttonQuestion.selected = selected
            } else {
                buttonQuestion.selected = false
            }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}


extension MedicalHistoryFormViewController : UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView.tag == 1 ? patient.medicalHistoryQuestions2.count : patient.medicalHistoryQuestions3.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cellMedicalHistory", forIndexPath: indexPath) as! MedicalHistoryCollectionViewCell
        let obj = collectionView.tag == 1 ? patient.medicalHistoryQuestions2[indexPath.item] : patient.medicalHistoryQuestions3[indexPath.item]
        cell.configureCellOption(obj)
        return cell
    }
}

extension MedicalHistoryFormViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            return patient.medicalHistoryQuestions1.count
        } else {
            let currentIndex = tableView.tag - 2
            let objects = patient.medicalHistoryQuestions4.filter { (obj) -> Bool in
                return obj.index >= (currentIndex * 19) && obj.index < ((currentIndex + 1) * 19)
            }
            return tableView.tag == 5 ? objects.count + 1 : objects.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView.tag == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellMedicalHistoryFrom1", forIndexPath: indexPath) as! MedicalHistoryFormTableViewCell1
            let obj = patient.medicalHistoryQuestions1[indexPath.row]
            cell.configureCell(obj)
            return cell
        } else {
            let currentIndex = tableView.tag - 2
            let cell = tableView.dequeueReusableCellWithIdentifier("cellMedicalHistoryFrom4", forIndexPath: indexPath) as! MedicalHistoryStep1TableViewCell
            let index = (currentIndex * 19) + indexPath.row
            let obj = patient.medicalHistoryQuestions4[index]
            cell.configureCellOption(obj)
            let stringObj = ["AIDS/HIV Positive", "Artificial heart valve", "Artificial Joints", "Cancer", "Chemotherapy", "Diabetes", "Drug Addiction", "Excessive Bleeding", "Heart Attack / Failure", "Heart Pacemaker", "Heart Trouble / Disease", "Hepatites A", "Hepatites B or C", "High Blood Pressure", "Tuberculosis"]
            if let selected = obj.isSelected {
                cell.labelTitle.textColor = selected && stringObj.contains(obj.question) ? UIColor.redColor() : UIColor.blackColor()
                cell.labelTitle.font = UIFont(name:selected ? "WorkSans-Medium" :  "WorkSans-Regular", size: cell.labelTitle.font.pointSize)
                cell.buttonYes.selected = selected
            } else {
                cell.labelTitle.textColor = UIColor.blackColor()
                cell.labelTitle.font = UIFont(name: "WorkSans-Regular", size: cell.labelTitle.font.pointSize)
                cell.buttonYes.selected = false
            }
            return cell
        }
    }
}
