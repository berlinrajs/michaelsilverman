//
//  MedicalHistoryStep5ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/29/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryStep5ViewController: PDViewController {
    
    
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: PDLabel!
    @IBOutlet weak var radioButton: RadioButton!
    @IBOutlet var viewPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textViewOthers: PDTextView!
    @IBOutlet weak var textViewComments: PDTextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signatureView.layer.cornerRadius = 3.0
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(MedicalHistoryStep5ViewController.setDateOnLabel))
        tapGesture.numberOfTapsRequired = 1
        labelDate.addGestureRecognizer(tapGesture)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setDateOnLabel() {
        labelDate.text = patient.dateToday
        labelDate.textColor = UIColor.blackColor()
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if labelDate.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            if textViewComments.text != "COMMENTS" {
                patient.comments = textViewComments.text
            } else {
                patient.comments = nil
            }
            patient.signature1 = signatureView.signatureImage()
            let medicalHistoryFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kMedicalHistoryFormVC") as! MedicalHistoryFormViewController
            medicalHistoryFormVC.patient = patient
            self.navigationController?.pushViewController(medicalHistoryFormVC, animated: true)
        }
    }
    
    func showPopup(tag : Int) {
        let frameSize = CGRectMake(0, 0, 348, 174)
        self.viewPopup.frame = frameSize
        self.viewPopup.center = self.view.center
        self.viewShadow.addSubview(self.viewPopup)
        self.viewPopup.transform = CGAffineTransformMakeScale(0.1, 0.1)
        textViewOthers.text = "IF YES TYPE HERE"
        textViewOthers.tag = tag
        textViewOthers.textColor = UIColor.lightGrayColor()
        self.viewShadow.hidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewPopup.transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionYesNo(sender: RadioButton) {
        if sender.tag == 2 {
            textViewOthers.resignFirstResponder()
            self.viewPopup.removeFromSuperview()
            self.viewShadow.hidden = true
            patient.otherIllness = nil
        } else {
            showPopup(2)
        }
    }
    
    @IBAction func buttonActionPopupOk(sender: AnyObject) {
        if !textViewOthers.isEmpty && textViewOthers.text != "IF YES TYPE HERE" {
            patient.otherIllness = textViewOthers.text
        } else {
            patient.otherIllness = nil
            radioButton.setSelectedWithTag(2)
        }
        textViewOthers.resignFirstResponder()
        self.viewPopup.removeFromSuperview()
        self.viewShadow.hidden = true
    }
}

extension MedicalHistoryStep5ViewController : UITextViewDelegate {
    
    
    func textViewDidBeginEditing(textView: UITextView) {
        
        if textView == textViewOthers {
            if textView.text == "IF YES TYPE HERE" {
                textView.text = ""
                textView.textColor = UIColor.blackColor()
            }
        } else {
            if textView.text == "COMMENTS" {
                textView.text = ""
                textView.textColor = UIColor.blackColor()
            }
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = textView == textViewOthers ? "IF YES TYPE HERE" : "COMMENTS"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
