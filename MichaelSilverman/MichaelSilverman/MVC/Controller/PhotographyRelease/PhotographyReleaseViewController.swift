//
//  PhotographyReleaseViewController.swift
//  AceDental
//
//  Created by Leojin Bose on 3/15/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PhotographyReleaseViewController: PDViewController {

    
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var textfieldRelationship : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signatureView.layer.cornerRadius = 3.0
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PhotographyReleaseViewController.setDateOnLabel))
        tapGesture.numberOfTapsRequired = 1
        labelDate.addGestureRecognizer(tapGesture)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setDateOnLabel() {
        labelDate.text = patient.dateToday
        labelDate.textColor = UIColor.blackColor()
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if labelDate.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.signature1 = signatureView.signatureImage()
            patient.parentRelation = textfieldRelationship.isEmpty ? nil : textfieldRelationship.text

            let photographyReleaseFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kPhotographyReleaseFormVC") as! PhotographyReleaseFormViewController
            photographyReleaseFormVC.patient = self.patient
            self.navigationController?.pushViewController(photographyReleaseFormVC, animated: true)
        }
    }


}

extension PhotographyReleaseViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

