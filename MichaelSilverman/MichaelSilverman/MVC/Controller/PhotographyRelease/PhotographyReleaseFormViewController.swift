//
//  PhotographyReleaseFormViewController.swift
//  AceDental
//
//  Created by Leojin Bose on 3/15/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PhotographyReleaseFormViewController: PDViewController {
    
    @IBOutlet var labelDate: UILabel!
    @IBOutlet var imageViewSignature: UIImageView!
    @IBOutlet weak var labelRelationship: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewSignature.image = patient.signature1
        labelDate.text = patient.dateToday
        let patientName = patient.fullName//"\(patient.firstName) \(patient.lastName)"
        labelDetails.text = labelDetails.text?.stringByReplacingOccurrencesOfString("(patient name)", withString: patientName)
        labelName.text = patientName
        if let relation = patient.parentRelation {
            labelRelationship.text = relation
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
