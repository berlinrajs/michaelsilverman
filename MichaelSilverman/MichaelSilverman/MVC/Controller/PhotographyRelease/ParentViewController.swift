//
//  ParentViewController.swift
//  MichaelSilverman
//
//  Created by Bala Murugan on 9/12/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ParentViewController: PDViewController {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    @IBOutlet weak var textFieldDateOfBirth: PDTextField!
    @IBOutlet weak var buttonYes: UIButton!
    @IBOutlet weak var buttonNo: UIButton!
    @IBOutlet weak var buttonSkip: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.text = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        if buttonYes.selected {
            if textFieldFirstName.isEmpty {
                let alert = Extention.alert("PLEASE ENTER FIRST NAME")
                self.presentViewController(alert, animated: true, completion: nil)
            } else if textFieldLastName.isEmpty {
                let alert = Extention.alert("PLEASE ENTER LAST NAME")
                self.presentViewController(alert, animated: true, completion: nil)
            } else if textFieldDateOfBirth.isEmpty {
                let alert = Extention.alert("PLEASE ENTER RELATIONSHIP")
                self.presentViewController(alert, animated: true, completion: nil)
            } else {
                patient.parentFirstName = textFieldFirstName.text
                patient.parentLastName = textFieldLastName.text
                patient.parentRelation = textFieldDateOfBirth.text
                gotoVC()
            }
        } else {
            patient.parentFirstName = nil
            patient.parentLastName = nil
            patient.parentRelation = nil
            gotoVC()
        }
    }
    @IBAction func skipAction(sender: AnyObject) {
        patient.parentFirstName = nil
        patient.parentLastName = nil
        patient.parentRelation = nil
        gotoVC()
    }
    
    func gotoVC() {
        let photographyReleaseVC = newConsentStoryboard.instantiateViewControllerWithIdentifier("kPhotographyReleaseVC") as! PhotographyReleaseViewController
        photographyReleaseVC.patient = self.patient
        self.navigationController?.pushViewController(photographyReleaseVC, animated: true)
    }
    
    @IBAction func buttonActionYesNo(sender: UIButton) {
        sender.selected = true
        if sender == buttonYes {
            buttonNo.selected = false
            textFieldFirstName.enabled = true
            textFieldLastName.enabled = true
            textFieldDateOfBirth.enabled = true
        } else {
            buttonYes.selected = false
            textFieldFirstName.enabled = false
            textFieldLastName.enabled = false
            textFieldDateOfBirth.enabled = false
            textFieldFirstName.text = ""
            textFieldLastName.text = ""
            textFieldDateOfBirth.text = ""
        }
    }
    

}
