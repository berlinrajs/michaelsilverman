//
//  GeneralConsentFormViewController.swift
//  SecureDental
//
//  Created by Leojin Bose on 5/30/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class GeneralConsentFormViewController: PDViewController {


    @IBOutlet weak var signatureView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.text = patient.dateToday
        labelName.text = patient.fullName//patient.firstName + " " + patient.lastName
        signatureView.image = patient.signature1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
