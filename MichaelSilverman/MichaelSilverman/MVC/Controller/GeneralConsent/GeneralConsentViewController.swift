//
//  GeneralConsentViewController.swift
//  SecureDental
//
//  Created by Leojin Bose on 5/30/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class GeneralConsentViewController: PDViewController {


    
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var dateLabel: DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateLabel.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !dateLabel.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.signature1 = signatureView.signatureImage()
            let formVC = self.storyboard?.instantiateViewControllerWithIdentifier("kGeneralConsentFormVC") as! GeneralConsentFormViewController
            formVC.patient = patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
