//
//  ConsentToOrthodonticTreatmentFormViewController.swift
//  AL-ShifaDentistry
//
//  Created by SRS Web Solutions on 20/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ConsentToOrthodonticTreatmentFormViewController: PDViewController {
    
    @IBOutlet var labelPatientName: UILabel!
    
    @IBOutlet var labelDate: UILabel!
  
    @IBOutlet var imageViewSignature1: UIImageView!
    
    @IBOutlet var imageViewSignature2: UIImageView!
    
    @IBOutlet weak var PatientSignName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewSignature1.image = patient.signature1
        imageViewSignature2.image = patient.signature2
        labelPatientName.text = patient.fullName//("\(patient.firstName) \(patient.lastName)")
        self.labelDate.text = patient.dateToday
        
        PatientSignName.text = patient.fullName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
