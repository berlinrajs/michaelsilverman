//
//  ConsentToOrthodonticTreatmentViewControllerScene.swift
//  AL-ShifaDentistry
//
//  Created by SRS Web Solutions on 20/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ConsentToOrthodonticTreatmentViewControllerScene: PDViewController {
    
    
    @IBOutlet var imageViewSignature2: SignatureView!

    @IBOutlet var imageViewSignature3: SignatureView!
    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var labelDate2: DateLabel!

    @IBOutlet weak var patietnSignName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        
        patietnSignName.text = patient.fullName
    }

    
    @IBAction func buttonActionNext(sender: AnyObject) {
        if !imageViewSignature2.isSigned() || !imageViewSignature3.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else {
            
            let nextViewControllerObj = self.storyboard?.instantiateViewControllerWithIdentifier("ConsentToOrthodonticTreatmentFormViewController") as! ConsentToOrthodonticTreatmentFormViewController
            patient.signature1 = imageViewSignature2.image
            patient.signature2 = imageViewSignature3.image
            nextViewControllerObj.patient = self.patient
            self.navigationController?.pushViewController(nextViewControllerObj, animated: true)
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
