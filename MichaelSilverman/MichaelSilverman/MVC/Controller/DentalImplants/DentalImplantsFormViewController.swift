//
//  DentalImplantsFormViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Office on 2/24/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DentalImplantsFormViewController: PDViewController {

    var textRanges : [NSRange]! = [NSRange]()
    
    @IBOutlet var labelDate: UILabel!
    @IBOutlet var imageViewSignature: UIImageView!
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var viewSignature: UIView!
    @IBOutlet var labelText1: UILabel!
    @IBOutlet var labelText2: UILabel!
    @IBOutlet var labelText3: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageViewSignature.image = patient.signature1
        labelDate.text = patient.dateToday
        
        
        
        var patientInfo = "Part 1 -Patient & Doctor Information\n\nPatient Name:"
        
        let patientName = patient.fullName//getText("\(patient.firstName) \(patient.lastName)")
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + "\nD.O.B: \(getText(patient.dateOfBirth))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateOfBirth)))
        
        patientInfo = patientInfo + "\nDoctor Name: \(getText("Dr. Michael Silverman"))"
        textRanges.append(patientInfo.rangeOfText(getText("Dr. Michael Silverman")))
        
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range)
        }
        
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(float: 0)], range: NSMakeRange(0, attributedString.length))
        labelDetails.attributedText = attributedString

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        scrollView.contentSize = CGSizeMake(screenSize.width, screenSize.height * 4)
        
        var viewSignatureFrame = self.viewSignature.frame
        viewSignatureFrame.origin.y = (screenSize.height * 4) - 120
        viewSignatureFrame.origin.x = 60.0
        viewSignatureFrame.size.width = screenSize.width - 120.0
        viewSignature.frame = viewSignatureFrame
        self.scrollView.addSubview(viewSignature)
        
        let labelFrame1 = CGRectMake(30.0, screenSize.height + 30.0, screenSize.width - 60.0, screenSize.height - 60.0)
        let labelFrame2 = CGRectMake(30.0, (screenSize.height * 2) + 30.0, screenSize.width - 60.0, screenSize.height - 60.0)
        let labelFrame3 = CGRectMake(30.0, (screenSize.height * 3) + 30.0, screenSize.width - 60.0, screenSize.height - 180.0)

        labelText1.frame = labelFrame1
        labelText2.frame = labelFrame2
        labelText3.frame = labelFrame3
        scrollView.addSubview(labelText1)
        scrollView.addSubview(labelText2)
        scrollView.addSubview(labelText3)

    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DentalImplantsFormViewController : UIScrollViewDelegate {
    
}
