//
//  DentalImplantsViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Office on 2/24/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DentalImplantsViewController: PDViewController {
    
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: PDLabel!
    @IBOutlet weak var labelName: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
        labelName.text = patient.fullName//"\(patient.firstName) \(patient.lastName)"
        signatureView.layer.cornerRadius = 3.0
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(DentalImplantsViewController.setDateOnLabel))
        tapGesture.numberOfTapsRequired = 1
        labelDate.addGestureRecognizer(tapGesture)
        

        // Do any additional setup after loading the view.
    }

    func setDateOnLabel() {
        labelDate.text = patient.dateToday
        labelDate.textColor = UIColor.blackColor()
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if labelDate.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.signature1 = signatureView.signatureImage()
            let dentalImplaintsFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentalImplantsFormVC") as! DentalImplantsFormViewController
            dentalImplaintsFormVC.patient = patient
            self.navigationController?.pushViewController(dentalImplaintsFormVC, animated: true)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
