//
//  AuthorizationFormViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Office on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AuthorizationFormViewController: PDViewController {

    var textRanges : [NSRange]! = [NSRange]()

    @IBOutlet weak var labelReason: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelPatientDetails: UILabel!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet weak var labelSignedPerson: UILabel!
    @IBOutlet var labelClinicName: [UILabel]!
    
    @IBOutlet weak var constraintReasonHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintPatientInfoHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        patient.newClinicName.setTextForArrayOfLabels(labelClinicName)
        
        imageViewSignature.image = patient.signature1
        labelDate.text = patient.dateToday
        
        var patientInfo = "\n I"
        
        let patientName = patient.fullName//getText("\(patient.firstName) \(patient.lastName)")
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " authorize Previous Clinic Name: \(getText(patient.previousClinicName))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.previousClinicName)))
        
        patientInfo = patientInfo + " New Clinic Name: \(getText(patient.newClinicName))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.newClinicName)))
        
        patientInfo = patientInfo + " Phone Number: \(getText(patient.phoneNumber))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.phoneNumber)))
        
        labelSignedPerson.text = is18YearsOld ? "PATIENT" : "PARENT/GUARDIAN"
        
        let text = getText("N/A")
        
        if let fax = patient.faxNumber {
            patientInfo = patientInfo + " Fax Number: \(getText(fax))"
            textRanges.append(patientInfo.rangeOfText(getText(fax)))
        } else {
            patientInfo = patientInfo + " Fax Number: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))
        }
        
        patientInfo = patientInfo + " Patient Name: \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " D.O.B: \(getText(patient.dateOfBirth))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateOfBirth)))
        
        patientInfo = patientInfo + " to disclose and provide copies of any and all clinical treatment, records, and information concerning my care (or child's if patient is under 18 years of age) which is in the clinic's possession to be sent to the following dentist or entity:"
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range)
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 10
        paragraphStyle.alignment = NSTextAlignment.Justified
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(float: 0), NSParagraphStyleAttributeName : paragraphStyle], range: NSMakeRange(0, attributedString.length))
        labelPatientDetails.attributedText = attributedString
        
        
        constraintPatientInfoHeight.constant = labelPatientDetails.attributedText!.heightWithConstrainedWidth(screenSize.width - 60.0) * 2
        
        let reason = patient.reasonForTransfer == nil ? "N/A" : patient.reasonForTransfer!
        let patientReason = "Reason for Transfer: \(getText(reason))"
        let attributedStr = NSMutableAttributedString(string: patientReason)
        attributedStr.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: NSMakeRange(patientReason.characters.count - getText(reason).characters.count, getText(reason).characters.count))
        attributedStr.addAttributes([NSBaselineOffsetAttributeName: NSNumber(float: 0), NSParagraphStyleAttributeName : paragraphStyle], range: NSMakeRange(0, attributedStr.length))
        labelReason.attributedText = attributedStr
        constraintReasonHeight.constant = labelReason.attributedText!.heightWithConstrainedWidth(screenSize.width - 60.0) * 2

    }
    
    var is18YearsOld : Bool {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        let birthDate = dateFormatter.dateFromString(patient.dateOfBirth.capitalizedString)
        let ageComponents = NSCalendar.currentCalendar().components(.Year, fromDate: birthDate!, toDate: NSDate(), options: NSCalendarOptions(rawValue: 0))
        return ageComponents.year >= 18
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
