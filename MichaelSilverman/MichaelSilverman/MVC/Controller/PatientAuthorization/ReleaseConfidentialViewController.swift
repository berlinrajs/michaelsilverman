//
//  ReleaseConfidentialViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Office on 2/21/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ReleaseConfidentialViewController: PDViewController {

    @IBOutlet weak var labelDate: PDLabel!
    @IBOutlet weak var signatureView: SignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signatureView.layer.cornerRadius = 3.0
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ReleaseConfidentialViewController.setDateOnLabel))
        tapGesture.numberOfTapsRequired = 1
        labelDate.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    func setDateOnLabel() {
        labelDate.text = patient.dateToday
        labelDate.textColor = UIColor.blackColor()
    }
    
    
    @IBAction func buttonActionNext(sender : AnyObject) {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if labelDate.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            
            patient.signature1 = signatureView.signatureImage()
            let authorizationFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kAuthorizationFormVC") as! AuthorizationFormViewController
            authorizationFormVC.patient = patient
            self.navigationController?.pushViewController(authorizationFormVC, animated: true)
        }
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}



