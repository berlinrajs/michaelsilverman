//
//  PatientAuthorizationViewController.swift
//  Michael Silverman Dentistry
//
//  Created by Office on 2/21/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientAuthorizationViewController: PDViewController {


    @IBOutlet weak var textFieldFaxNumber: PDTextField!
    @IBOutlet weak var textFieldNewClinic: PDTextField!
    @IBOutlet weak var textFieldPatientNumber: PDTextField!
    @IBOutlet weak var textFieldPhoneNumber: PDTextField!
    @IBOutlet weak var textViewReasonForTransfer: UITextView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonActionNext(sender : AnyObject) {
        self.view.endEditing(true)
        if textFieldNewClinic.isEmpty {
            let alert = Extention.alert("PLEASE ENTER NEW CLINIC NAME")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldPhoneNumber.isEmpty && !textFieldPhoneNumber.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !textFieldFaxNumber.isEmpty && !textFieldFaxNumber.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID FAX NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.previousClinicName = "Michael Silverman Dentistry"
            patient.newClinicName = textFieldNewClinic.text
            if !textFieldPatientNumber.isEmpty {
                patient.patientNumber = textFieldPatientNumber.text
            } else {
                patient.patientNumber = nil
            }
            if !textFieldFaxNumber.isEmpty {
                patient.faxNumber = textFieldFaxNumber.text
            } else {
                patient.faxNumber = nil
            }
            if textViewReasonForTransfer.text != "PLEASE TYPE REASON FOR TRANSFER" {
                patient.reasonForTransfer = textViewReasonForTransfer.text
            } else {
                patient.reasonForTransfer = nil
            }
            patient.phoneNumber = textFieldPhoneNumber.isEmpty ? "" : textFieldPhoneNumber.text
            patient.faxNumber = textFieldFaxNumber.text
            let releaseConfidentialVC = self.storyboard?.instantiateViewControllerWithIdentifier("kReleaseConfidentialVC") as! ReleaseConfidentialViewController
            releaseConfidentialVC.patient = patient
            self.navigationController?.pushViewController(releaseConfidentialVC, animated: true)
        }
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

}


extension PatientAuthorizationViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "PLEASE TYPE REASON FOR TRANSFER" {
            textView.text = ""
            textView.textColor = UIColor.whiteColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == "" {
            textView.text = "PLEASE TYPE REASON FOR TRANSFER"
            textView.textColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension PatientAuthorizationViewController : UITextFieldDelegate {
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldPhoneNumber || textField == textFieldFaxNumber {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
