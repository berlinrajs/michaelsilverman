//
//  InsuranceReleaseFormVC.swift
//  MichaelSilverman
//
//  Created by Leojin Bose on 10/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class InsuranceReleaseFormVC: PDViewController {

    @IBOutlet var labelDate: [UILabel]!
    @IBOutlet weak var imageViewSignature1: UIImageView!
    @IBOutlet weak var imageViewSignature2: UIImageView!
    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelDOB: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        labelDOB.text = patient.dateOfBirth
        imageViewSignature1.image = patient.signature1
        imageViewSignature2.image = patient.signature2
        for label in labelDate {
            label.text = patient.dateToday
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
