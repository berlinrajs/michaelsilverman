//
//  InsuranceReleaseViewController.swift
//  MichaelSilverman
//
//  Created by Leojin Bose on 10/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class InsuranceReleaseViewController: PDViewController {

    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var signatureView2: SignatureView!
    @IBOutlet var labelDate: [DateLabel]!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        for date in labelDate {
            date.todayDate = patient.dateToday
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        if let _ = findEmptySignature() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if let _ = findEmptyDate() {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.signature1 = signatureView1.signatureImage()
            patient.signature2 = signatureView2.signatureImage()
            let insuranceReleaseFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kInsuranceReleaseFormVC") as! InsuranceReleaseFormVC
            insuranceReleaseFormVC.patient = patient
            self.navigationController?.pushViewController(insuranceReleaseFormVC, animated: true)
        }
    }
    
    func findEmptyDate() -> DateLabel? {
        for date in labelDate {
            if !date.dateTapped  {
                return date
            }
        }
        return nil
    }
    
    func findEmptySignature() -> SignatureView? {
        for signatureView in [signatureView1, signatureView2] {
            if !signatureView.isSigned()  {
                return signatureView
            }
        }
        return nil
    }

}
