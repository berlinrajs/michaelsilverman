//
//  CrownsAndBridgesViewControllerScene.swift
//  AdultMedicalForm
//
//  Created by SRS Web Solutions on 17/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CrownsAndBridgesViewControllerScene: PDViewController {
    @IBOutlet var imgSignatureView: SignatureView!
    
    @IBOutlet var imgSignatureView2: SignatureView!
    
    @IBOutlet var lblDate1: DateLabel!
    
    
    @IBOutlet var lblDate2: DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        lblDate1.todayDate = patient.dateToday
        lblDate2.todayDate = patient.dateToday
    }
   
    
    @IBAction func btnActionNext(sender: AnyObject) {
        
         if !imgSignatureView.isSigned() || !imgSignatureView2.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if !lblDate1.dateTapped || !lblDate2.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
         }else {
        let nextViewControllerObj = self.storyboard?.instantiateViewControllerWithIdentifier("CrownsAndBridgesConsentFormViewController") as! CrownsAndBridgesConsentFormViewController
        patient.signature1 = imgSignatureView.image
        patient.signature2 = imgSignatureView2.image
        nextViewControllerObj.patient = self.patient

        self.navigationController?.pushViewController(nextViewControllerObj, animated: true)
        }}

override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
        }}


