//
//  CrownsAndBridgesConsentFormViewController.swift
//  AdultMedicalForm
//
//  Created by SRS Web Solutions on 17/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CrownsAndBridgesConsentFormViewController: PDViewController {
    
    @IBOutlet var imgViewSign: UIImageView!
    
    @IBOutlet var imgViewSign2: UIImageView!
    
    @IBOutlet var lblDate1: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var lblDate2: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pdfView = scrollView
        imgViewSign.image = patient.signature1
        imgViewSign2.image = patient.signature2
        lblDate1.text = patient.dateToday
         lblDate2.text = patient.dateToday
        

        // Do any additional setup after loading the view.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
